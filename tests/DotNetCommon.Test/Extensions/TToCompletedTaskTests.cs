﻿using DotNetCommon.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class TToCompletedTaskTests
    {
        [Test]
        public void Test()
        {
            var i = 2;
            var task = i.ToCompletedTask();
            task.IsCompleted.ShouldBeTrue();
            task.Result.ShouldBe(2);
        }
    }
}
