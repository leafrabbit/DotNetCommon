﻿namespace DotNetCommon.Test.Extensions
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using Shouldly;
    using DotNetCommon.Extensions;
    using System;
    using System.Linq;

    [TestFixture]
    internal sealed class ListExtensionsTests
    {
        [Test]
        public void When_initializing_a_list_with_multiple_items()
        {
            var list = new List<int>
            {
                1,
                new[] {2, 3, 4, 5},
                6
            };

            list.Count.ShouldBe(6);
            list.ShouldBe(new[] { 1, 2, 3, 4, 5, 6 });
        }

        [Test]
        public void ListRemoveTest()
        {
            var list = new List<int> { 1, 2, 3, 4, 4, 5, 6 };
            list.Remove(i => i >= 4);
            list.Count.ShouldBe(3);
        }

        [Test]
        public void ToDictionaryTest()
        {
            var list = new List<Person>()
            {
                new Person(){Id=1,Name="张三",Birth=new DateTime(1990,01,01)},
                new Person(){Id=2,Name="李四",Birth=new DateTime(1991,01,01)},
            };
            var dic = list.ToDictionary(i => i.Id, i => i.Name);
            dic.Count.ShouldBe(2);
            dic[1].ShouldBe("张三");
            dic[2].ShouldBe("李四");
        }

        [Test]
        public void RemoveTest()
        {
            var list = new List<Person>()
            {
                new Person(){Id=1,Name="张三",Birth=new DateTime(1990,01,01)},
                new Person(){Id=2,Name="李四",Birth=new DateTime(1991,01,01)},
            };
            list.Remove(i => i.Id >= 1);
            list.Count.ShouldBe(0);
        }

        [Test]
        public void GetPageTest()
        {
            var list = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            var newList = list.GetPage(2, 2).ToList();
            newList.ShouldBe(new List<int>() { 3, 4 });
        }

        [Test]
        public void IsNotNullOrEmptyTest()
        {
            List<int> list = null;
            list.IsNotNullOrEmpty().ShouldBeFalse();
            list = new List<int>();
            list.IsNotNullOrEmpty().ShouldBeFalse();
            list = new List<int>() { 1 };
            list.IsNotNullOrEmpty().ShouldBeTrue();
        }

        [Test]
        public void IsNullOrEmptyTest()
        {
            List<int> list = null;
            list.IsNullOrEmpty().ShouldBeFalse();
            list = new List<int>();
            list.IsNotNullOrEmpty().ShouldBeFalse();
            list = new List<int>() { 1 };
            list.IsNotNullOrEmpty().ShouldBeTrue();
        }

        [Test]
        public void ToStringSeparatedTest()
        {
            var list = new List<int>() { 1, 2, 3 };
            list.ToStringSeparated(",").ShouldBe("1,2,3");
            var arr = new int[] { 1, 2 };
            arr.ToStringSeparated(",").ShouldBe("1,2");
        }

        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime Birth { get; set; }
        }
    }
}