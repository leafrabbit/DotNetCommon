﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class ObjectTests_To
    {
        /// <summary>
        /// 字符串转日期
        /// </summary>
        [Test]
        public void TestString2DateTime()
        {
            //字符串->DateTime
            "2020-01-01".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-01"));
            "2020-01-01 02:03:04.123".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-01 02:03:04.123"));
            "2020-01-02T01:08:07.123Z".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-02T01:08:07.123Z"));
            "2020-01-02 09:08:07 +08:00".To<DateTime>().ShouldBe(DateTime.Parse("2020-01-02 09:08:07 +08:00"));


            string str = null;
            str.To<DateTime?>().ShouldBe(null);

            //字符串->DateTimeOffset
            "2020-01-01".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-01"));
            "2020-01-01 02:03:04.123".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-01 02:03:04.123"));
            "2020-01-02T01:08:07.123Z".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-02T01:08:07.123Z"));
            "2020-01-02 09:08:07 +08:00".To<DateTimeOffset>().ShouldBe(DateTimeOffset.Parse("2020-01-02 09:08:07 +08:00"));

            //字符串->DateTime 指定格式
            "2021-05-09 23:16:03.503".To<DateTime>("yyyy-MM-dd HH:mm:ss.fff").ShouldBe(DateTime.Parse("2021-05-09 23:16:03.503"));
            "2021年的05月09日啊".To<DateTime>("yyyy年的MM月dd日啊").ShouldBe(DateTime.Parse("2021-05-09"));
            "2020-01-02 09:08:07.123 +08:00".To<DateTime>("yyyy-MM-dd HH:mm:ss.fff zzz").ShouldBe(DateTime.Parse("2020-01-02 09:08:07.123 +08:00"));
            "2020-01-02T01:08:07Z".To<DateTime>("yyyy-MM-ddThh:mm:ssZ").ShouldBe(DateTime.Parse("2020-01-02T01:08:07Z"));

            //字符串->DateTimeOffset 指定格式
            "2021-05-09 23:16:03.503".To<DateTimeOffset>("yyyy-MM-dd HH:mm:ss.fff").ShouldBe(DateTimeOffset.Parse("2021-05-09 23:16:03.503"));
            "2021年的05月09日啊".To<DateTimeOffset>("yyyy年的MM月dd日啊").ShouldBe(DateTimeOffset.Parse("2021-05-09"));
            "2020-01-02 09:08:07.123 +08:00".To<DateTimeOffset>("yyyy-MM-dd HH:mm:ss.fff zzz").ShouldBe(DateTimeOffset.Parse("2020-01-02 09:08:07.123 +08:00"));
            "2020-01-02T01:08:07Z".To<DateTimeOffset>("yyyy-MM-ddThh:mm:ssZ").ShouldBe(DateTimeOffset.Parse("2020-01-02T01:08:07Z"));
        }

        /// <summary>
        /// 字符串转bool
        /// </summary>
        [Test]
        public void TestStringToBool()
        {
            //字符串转bool
            //"true"、"ok"、"yes"、"1"转为true，其他转为false，不区分大小写
            var str = "true";
            str.To<bool>().ShouldBeTrue();
            str = "True";
            str.To<bool>().ShouldBeTrue();
            str = "false";
            str.To<bool>().ShouldBeFalse();
            str = "False";
            str.To<bool>().ShouldBeFalse();

            str = "Ok";
            str.To<bool>().ShouldBeTrue();
            str = "notOk";
            str.To<bool>().ShouldBeFalse();

            str = "yes";
            str.To<bool>().ShouldBeTrue();
            str = "no";
            str.To<bool>().ShouldBeFalse();

            str = "1";
            str.To<bool>().ShouldBeTrue();
            str = "0";
            str.To<bool>().ShouldBeFalse();

            str = null;
            str.To<bool?>().ShouldBeNull();
        }

        [Test]
        public void TestDateTimeToString()
        {
            var str = new DateTime(2020, 01, 02).To<string>("yyyy-MM");
            str.ShouldBe("2020-01");

            str = new DateTimeOffset(2020, 01, 02, 03, 04, 05, TimeSpan.FromHours(8)).To<string>("yyyy-MM");
            str.ShouldBe("2020-01");
        }

        /// <summary>
        /// 枚举转字符串
        /// </summary>
        [Test]
        public void TestEnumToString()
        {
            //枚举转字符串
            EnumState.Active.To<string>().ShouldBe("Active");
            (EnumState.Active | EnumState.Close).To<string>().ShouldBe("Close, Active");
        }

        /// <summary>
        /// 字符串转数字
        /// </summary>
        [Test]
        public void TestStringToNumber()
        {
            //字符串转数字
            var str = "1.2";
            str.To<double>().ShouldBe(1.2);
            str = null;
            str.To<double?>().ShouldBe(null);
            str.ToWithDefault<int>(0).ShouldBe(0);
            str = "12";
            str.To<int>().ShouldBe(12);
        }

        /// <summary>
        /// 字符串转枚举
        /// </summary>
        [Test]
        public void TestStringToEnum()
        {
            //字符串转枚举
            var str = "Close";
            str.To<EnumState>().ShouldBe(EnumState.Close);
            str = "Close,Active";
            var state = str.To<EnumState>();
            state.Contains(EnumState.Close).ShouldBeTrue();
            state.Contains(EnumState.Active).ShouldBeTrue();
            state.Contains(EnumState.Open).ShouldBeFalse();
        }

        /// <summary>
        /// 枚举转数字
        /// </summary>
        [Test]
        public void TestEnumToNumber()
        {
            EnumState.Open.To<int>().ShouldBe(1);
            EnumState.Close.To<int>().ShouldBe(2);
            (EnumState.Close | EnumState.Open).To<int>().ShouldBe(3);
        }

        /// <summary>
        /// 数字转枚举
        /// </summary>
        [Test]
        public void TestNumberToEnum()
        {
            1.To<EnumState>().ShouldBe(EnumState.Open);
            2.To<EnumState>().ShouldBe(EnumState.Close);
            3.To<EnumState>().ShouldBe(EnumState.Open | EnumState.Close);
            4.To<EnumState>().ShouldBe(EnumState.Active);
            7.To<EnumState>().ShouldBe(EnumState.Open | EnumState.Close | EnumState.Active);
        }

        /// <summary>
        /// 数字转数字
        /// </summary>
        [Test]
        public void TestNumberToNumber()
        {
            //decimal转double
            1.88m.To<double>().ShouldBe(1.88d);

            //double转decimal
            1.88d.To<decimal>().ShouldBe(1.88m);

            //int转double
            20.To<double>().ShouldBe(20.0d);

            //double转int
            1.88d.To<int>().ShouldBe(2);
        }

        [Flags]
        public enum EnumState { Open = 1, Close = 2, Active = 4 }

        [Test]
        public void TestFail()
        {
            var str = "lp1";
            str.ToWithDefault<int>(default(int)).ShouldBe(default(int));
            str.ToWithDefault<int>(2).ShouldBe(2);
        }

        [Test]
        public void TestClass()
        {
            var person = new Person();
            Shouldly.Should.Throw(() => person.To<Student>(), typeof(InvalidCastException));
        }

        public class Person { }

        public class Student { }

    }

}
