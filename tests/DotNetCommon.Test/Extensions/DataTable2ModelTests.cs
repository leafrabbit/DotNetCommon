﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using DotNetCommon.Extensions;
using System.Diagnostics;
using System.Threading;

namespace DotNetCommon.Test.Extensions
{
    /// <summary>
    /// DataTable和Model互转测试
    /// </summary>
    [TestFixture]
    public class DataTable2ModelTests
    {
        [Test]
        public void DataTable2ModelTest1()
        {
            var dt = GetDataTable();
            var st = new Stopwatch();
            st.Start();
            var persons = dt.ToModels<Person>();
            Console.WriteLine($"ToModels: {st.ElapsedMilliseconds}毫秒");
            st = new Stopwatch();
            st.Start();
            var dicList = dt.ToDictionaryList();
            Console.WriteLine($"ToDictionaryList: {st.ElapsedMilliseconds}毫秒");
        }
        private DataTable GetDataTable()
        {
            var dt = new DataTable();
            dt.Columns.Add("PByte");
            dt.Columns.Add("PByteNull");
            dt.Columns.Add("PSByte");
            dt.Columns.Add("PSByteNull");
            dt.Columns.Add("PShort");
            dt.Columns.Add("PShortNull");
            dt.Columns.Add("PUShort");
            dt.Columns.Add("PUShortNull");
            dt.Columns.Add("PInt");
            dt.Columns.Add("PIntNull");
            dt.Columns.Add("PUInt");
            dt.Columns.Add("PUIntNull");
            dt.Columns.Add("PLong");
            dt.Columns.Add("PLongNull");
            dt.Columns.Add("PULong");
            dt.Columns.Add("PULongNull");
            dt.Columns.Add("PFloat");
            dt.Columns.Add("PFloatNull");
            dt.Columns.Add("PDouble");
            dt.Columns.Add("PDoubleNull");
            dt.Columns.Add("PDecimal");
            dt.Columns.Add("PDecimalNull");
            dt.Columns.Add("PChar");
            dt.Columns.Add("PCharNull");
            dt.Columns.Add("PString");
            dt.Columns.Add("PBool");
            dt.Columns.Add("PBoolNull");
            dt.Columns.Add("PDateTime");
            dt.Columns.Add("PDateTimeNull");
            dt.Columns.Add("PDateTimeOffset");
            dt.Columns.Add("PDateTimeOffsetTimeNull");
            dt.Columns.Add("PDBNull");
            for (var i = 0; i < 100; i++)
            {
                var row = dt.NewRow();
                row["PByte"] = 0x01 + i;
                row["PByteNull"] = null;
                row["PSByte"] = 0x02 + i;
                row["PSByteNull"] = null;
                row["PShort"] = 0x03 + i;
                row["PShortNull"] = null;
                row["PUShort"] = 0x04 + i;
                row["PUShortNull"] = null;
                row["PInt"] = 5 + i;
                row["PIntNull"] = null;
                row["PUInt"] = 6 + i;
                row["PUIntNull"] = null;
                row["PLong"] = 7 + i;
                row["PLongNull"] = null;
                row["PULong"] = 8 + i;
                row["PULongNull"] = null;
                row["PFloat"] = 9.23 + i;
                row["PFloatNull"] = null;
                row["PDouble"] = 10.23 + i;
                row["PDoubleNull"] = null;
                row["PDecimal"] = 11.23 + i;
                row["PDecimalNull"] = null;
                row["PChar"] = 'p';
                row["PCharNull"] = null;
                row["PString"] = "PString";
                row["PBool"] = true;
                row["PBoolNull"] = null;
                row["PDateTime"] = DateTime.Now;
                row["PDateTimeNull"] = null;
                row["PDateTimeOffset"] = DateTimeOffset.Now;
                row["PDateTimeOffsetTimeNull"] = null;
                if (i == 0) row["PDBNull"] = DBNull.Value;
                else row["PDBNull"] = null;
                dt.Rows.Add(row);
            }
            return dt;
        }

        #region model
        public class Person
        {
            public byte PByte { get; set; }
            public byte? PByteNull { get; set; }
            public sbyte PSByte { get; set; }
            public sbyte? PSByteNull { get; set; }

            public short PShort { get; set; }
            public short? PShortNull { get; set; }
            public ushort PUShort { get; set; }
            public ushort? PUShortNull { get; set; }

            public int PInt { get; set; }
            public int? PIntNull { get; set; }
            public uint PUInt { get; set; }
            public uint? PUIntNull { get; set; }

            public long PLong { get; set; }
            public long? PLongNull { get; set; }
            public ulong PULong { get; set; }
            public ulong? PULongNull { get; set; }

            public float PFloat { get; set; }
            public float? PFloatNull { get; set; }

            public double PDouble { get; set; }
            public double? PDoubleNull { get; set; }

            public decimal PDecimal { get; set; }
            public decimal? PDecimalNull { get; set; }

            public char PChar { get; set; }
            public char? PCharNull { get; set; }
            public string PString { set; get; }
            public bool PBool { get; set; }
            public bool? PBoolNull { get; set; }

            public DateTime PDateTime { get; set; }
            public DateTime? PDateTimeNull { get; set; }

            public DateTimeOffset PDateTimeOffset { get; set; }
            public DateTimeOffset? PDateTimeOffsetTimeNull { get; set; }

            public DBNull PDBNull { get; set; }
        }
        #endregion
    }
}
