﻿using DotNetCommon.Serialize;
using Newtonsoft.Json;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Extensions
{
    [TestFixture]
    public class JsonExtensionTests
    {
        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                Age = 18,
                Name = "小明",
                Birth = DateTime.Now
            };
            //{"Id":"1","Age":18,"Name":"小明","Birth":"2021-05-25 21:04:43"}
            var json = person.ToJsonFast(isLongToString: true);

            //{"Id":"1","Age":"18","Name":"小明","Birth":"2021-05-25"}
            json = person.ToJsonFast(isAllToString: true, dateFormatString: "yyyy-MM-dd");

        }

        public class Person
        {
            public long Id { get; set; }
            public int Age { get; set; }
            public string Name { get; set; }
            public DateTime Birth { get; set; }
        }
    }


}
