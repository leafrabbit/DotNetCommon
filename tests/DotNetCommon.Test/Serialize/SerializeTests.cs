﻿using DotNetCommon.Serialize;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Serialize
{
    [TestFixture]
    internal sealed class SerializeTests
    {
        /// <summary>
        /// NumberConverter、DateTimeConverter
        /// </summary>
        [Test]
        public void ToJsonTest()
        {
            var obj = new Person()
            {
                Id = long.MaxValue,
                Name = "小明",
                State = EnumState.Close,
                Birth = DateTime.Now,
                Addr = "天明路"
            };
            var str = obj.ToJson();
        }

        /// <summary>
        /// PrimitiveConvertor
        /// </summary>
        [Test]
        public void ToJsonFastTest()
        {
            var person = new Person2()
            {
                PSByte = (sbyte)-1,
                PSByteNull = (sbyte?)-2,
                PByte = (byte)1,
                PByteNull = (byte?)2,

                PShort = (short)3,
                PShortNull = (short?)4,
                PUShort = (ushort)5,
                PUShortNull = (ushort)6,

                PInt = 7,
                PUInt = 8,
                PIntNull = 9,
                PUIntNull = 10,

                PLong = 11,
                PULong = 12,
                PLongNull = 13,
                PULongNull = 14,

                PFloat = 15.1f,
                PUFloat = 16.1234567890f,

                PDouble = 17.1234567890,
                PDoubleNull = 18.12345678901234567890,

                PDecimal = 19.12345678901234567890m,
                PDecimalNull = 20.123456789012345678901234567890m,

                PChar = 'a',
                PCharNull = 'b',

                PBool = false,
                PBoolNull = null,

                PString = "abc",

                PDateTime = DateTime.Now,
                PDateTimeNull = DateTime.UtcNow,

            };
            var str = Newtonsoft.Json.JsonConvert.SerializeObject(person, Formatting.Indented);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(person, Formatting.Indented, new JsonSerializerSettings()
            {
                Converters = new List<JsonConverter>()
                {
                    new PrimitiveConvertor()
                }
            });
            str.ShouldBe(json);
        }

        #region Model1 & NumberConverter、DateTimeConverter
        public class Person
        {
            [JsonConverter(typeof(NumberConverter), NumberConverterShip.Int64)]
            public long Id { get; set; }

            [JsonConverter(typeof(DateTimeConverter), "yyyy-MM-dd")]
            public DateTime Birth { get; set; }

            [JsonIgnore]
            public string Addr { set; get; }

            [JsonProperty("OtherName")]
            public string Name { set; get; }

            [JsonProperty(DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate)]
            public int StudentId { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public int? ClassId { get; set; }

            [JsonConverter(typeof(StringEnumConverter))]
            public EnumState State { set; get; }
        }

        public enum EnumState
        {
            Active,
            Close,
            Open
        }
        #endregion
        #region Model2 & PrimitiveConvertor
        public class Person2
        {
            public byte PByte { get; set; }
            public byte? PByteNull { get; set; }
            public sbyte PSByte { get; set; }
            public sbyte? PSByteNull { get; set; }

            public short PShort { get; set; }
            public short? PShortNull { get; set; }
            public ushort PUShort { get; set; }
            public ushort? PUShortNull { get; set; }

            public int PInt { get; set; }
            public uint PUInt { get; set; }
            public int? PIntNull { get; set; }
            public uint? PUIntNull { get; set; }

            public long PLong { get; set; }
            public ulong PULong { get; set; }
            public long? PLongNull { get; set; }
            public ulong? PULongNull { get; set; }

            public float PFloat { get; set; }
            public float? PUFloat { get; set; }

            public double PDouble { get; set; }
            public double? PDoubleNull { get; set; }

            public decimal PDecimal { get; set; }
            public decimal? PDecimalNull { get; set; }

            public char PChar { get; set; }
            public char? PCharNull { get; set; }

            public bool PBool { get; set; }
            public bool? PBoolNull { get; set; }

            public string PString { get; set; }

            public DateTime PDateTime { get; set; }
            public DateTime? PDateTimeNull { get; set; }

            public EnumTest State { get; set; }

            public enum EnumTest
            {
                Open, Active, Dead
            }
        }
        #endregion
    }
}
