﻿using DotNetCommon.Test.Extensions;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class RandomHelperTests
    {
        [Test]
        public void RandomTest()
        {
            List<int> ints = new List<int>();
            100.Times(() =>
            {
                var r = RandomHelper.Next(1, 10);
                ints.Add(r);
                r.ShouldBeInRange(1, 10);
                ints.Count(_ => _ == r).ShouldBeLessThan(Math.Max(ints.Count, 10));
                var r2 = RandomHelper.Next(10);
                ints.Add(r2);
                r2.ShouldBeLessThan(10);
                ints.Count(_ => _ == r2).ShouldBeLessThan(Math.Max(ints.Count, 10));
            });

            List<double> doubles = new List<double>();
            100.Times(() =>
            {
                var r = RandomHelper.Next(-10, 100.53);
                doubles.Add(r);
                r.ShouldBeInRange(-10, 100.53);
                doubles.Count(_ => _ == r).ShouldBeLessThan(Math.Max(doubles.Count, 10));
                var r2 = RandomHelper.Next(100.53);
                doubles.Add(r2);
                r2.ShouldBeLessThan(100.53);
                doubles.Count(_ => _ == r2).ShouldBeLessThan(Math.Max(doubles.Count, 10));
            });

        }
    }
}
