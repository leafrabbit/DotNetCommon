﻿using DotNetCommon.Data;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DotNetCommon.Extensions;
using Shouldly;

namespace DotNetCommon.Test.Data
{
    [TestFixture]
    public class TreeTests
    {
        #region 模型
        public class Area
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? PId { get; set; }
            public List<Area> Children { get; set; }
        }

        public class Area2 : ITreeStruct<Area2>
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? PId { get; set; }
            public List<Area2> Children { get; set; }
        }

        public class Area3
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public int? PId { get; set; }
        }
        #endregion

        #region ToTree
        /// <summary>
        /// FetchToTree/ToTree
        /// </summary>
        [Test]
        public void TestToTree()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);

            //FetchToTree: ITreeStruct
            var list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area2>>(json);
            var tree2 = list2.FetchToTree(i => i.Id, i => i.PId);

            //ToTree: 普通
            var list3 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area3>>(json);
            var tree3 = list3.ToTree(i => i.Id, i => i.PId, i => i.PId == null);
            var tree4 = list3.ToTree(i => i.Id, i => i.PId, default(int?));
        }
        #endregion

        #region ToFlat
        /// <summary>
        /// ToFlat
        /// </summary>
        [Test]
        public void TestToFlat()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //ToFlat: SetNull/SetEmpty
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var flat = tree.ToFlat(i => i.Children);
            flat = tree.ToFlat(i => i.Children, TreeToFlatAction.SetEmpty);
            flat.ForEach(i =>
            {
                Assert.IsTrue(i.Children == null || i.Children.Count == 0);
            });

            //ToFlat: SetEmptyCollection
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            flat = tree.ToFlat(i => i.Children, TreeToFlatAction.SetEmptyCollection);
            flat.ForEach(i =>
            {
                Assert.IsTrue(i.Children != null && i.Children.Count == 0);
            });

            //ToFlat: SetNull
            list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            flat = tree.ToFlat(i => i.Children, TreeToFlatAction.SetNull);
            flat.ForEach(i =>
            {
                Assert.IsTrue(i.Children == null);
            });

            //FetchToTree: ITreeStruct
            var list2 = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area2>>(json);
            var tree2 = list2.FetchToTree(i => i.Id, i => i.PId);
            var flat2 = tree2.ToFlat(i => i.Children);
        }
        #endregion

        #region FilterTree
        /// <summary>
        /// FilterTree: withAllChildren-false, withAllParents-true
        /// </summary>
        [Test]
        public void TestFilterTreeDefault()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var res = tree.ToList().FilterTree(i => i.Children, i => i.Name.Contains("州"));

            var list2 = new List<string>();
            res.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                list2.Add($"{"--".Repeat(deep + 1)}{depart.Name}");
            });
            string.Join("\r\n", list2).ShouldBe(@"--中国
----河南
------郑州
--美国
----华盛顿州
----得克萨斯州");
        }

        /// <summary>
        /// FilterTree: withAllChildren-true, withAllParents-true
        /// </summary>
        [Test]
        public void TestFilterTreeWithAllChiren()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var res = tree.ToList().FilterTree(i => i.Children, i => i.Name.Contains("郑"), true);

            var list2 = new List<string>();
            res.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                list2.Add($"{"--".Repeat(deep + 1)}{depart.Name}");
            });
            string.Join("\r\n", list2).ShouldBe(@"--中国
----河南
------郑州
--------中原区
--------金水区");
        }

        /// <summary>
        /// FilterTree: withAllChildren-true, withAllParents-false
        /// </summary>
        [Test]
        public void TestFilterTreeWithAllChirenButNoParents()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0);
            var res = tree.ToList().FilterTree(i => i.Children, i => i.Name.Contains("郑"), true, false);

            var list2 = new List<string>();
            res.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                list2.Add($"{"--".Repeat(deep + 1)}{depart.Name}");
            });
            string.Join("\r\n", list2).ShouldBe(@"--郑州
----中原区
----金水区");
        }
        #endregion

        #region RecurseTree
        /// <summary>
        /// RecurseTree
        /// </summary>
        [Test]
        public void TestRecurseTree()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0).ToList();
            var list2 = new List<string>();

            var res = tree.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                var parents = ctx.Parents;
                bool b = ctx.IsLeaf;
                list2.Add($"{depart.Name}:{deep}:{b}:{parents.Select(i => i.Name).ToStringSeparated(",")}");
            });
            string.Join("\r\n", list2).ShouldBe(@"中国:0:False:
河南:1:False:中国
郑州:2:False:中国,河南
中原区:3:True:中国,河南,郑州
金水区:3:True:中国,河南,郑州
洛阳:2:False:中国,河南
西工区:3:True:中国,河南,洛阳
湖北:1:False:中国
武汉:2:True:中国,湖北
十堰:2:True:中国,湖北
山东:1:True:中国
美国:0:False:
华盛顿州:1:True:美国
得克萨斯州:1:True:美国");
        }

        /// <summary>
        /// RecurseTree: BreakRecurse
        /// </summary>
        [Test]
        public void TestRecurseTreeBreak()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0).ToList();
            var list2 = new List<string>();
            var res = tree.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                var parents = ctx.Parents;
                bool b = ctx.IsLeaf;
                list2.Add($"{depart.Name}:{deep}:{b}:{parents.Select(i => i.Name).ToStringSeparated(",")}");
                if (ctx.Current.Name == "武汉") ctx.BreakRecurse();
            });
            string.Join("\r\n", list2).ShouldBe(@"中国:0:False:
河南:1:False:中国
郑州:2:False:中国,河南
中原区:3:True:中国,河南,郑州
金水区:3:True:中国,河南,郑州
洛阳:2:False:中国,河南
西工区:3:True:中国,河南,洛阳
湖北:1:False:中国
武汉:2:True:中国,湖北");
        }

        /// <summary>
        /// RecurseTree: NextSibling
        /// </summary>
        [Test]
        public void TestRecurseNextSibling()
        {
            /*
            中国
                河南
                    郑州
                        中原区
                        金水区
                    洛阳
                        西工区
                湖北
                    武汉
                    十堰
                山东
            美国
                华盛顿州
                得克萨斯州
             */
            var json = "[{\"Id\":1,\"Name\":\"中国\",\"PId\":null,\"Children\":[]},{\"Id\":2,\"Name\":\"河南\",\"PId\":1,\"Children\":[]},{\"Id\":3,\"Name\":\"郑州\",\"PId\":2,\"Children\":[]},{\"Id\":4,\"Name\":\"中原区\",\"PId\":3,\"Children\":[]},{\"Id\":5,\"Name\":\"金水区\",\"PId\":3,\"Children\":[]},{\"Id\":6,\"Name\":\"洛阳\",\"PId\":2,\"Children\":[]},{\"Id\":7,\"Name\":\"西工区\",\"PId\":6,\"Children\":[]},{\"Id\":8,\"Name\":\"湖北\",\"PId\":1,\"Children\":[]},{\"Id\":9,\"Name\":\"武汉\",\"PId\":8,\"Children\":[]},{\"Id\":10,\"Name\":\"十堰\",\"PId\":8,\"Children\":[]},{\"Id\":11,\"Name\":\"山东\",\"PId\":1,\"Children\":[]},{\"Id\":12,\"Name\":\"美国\",\"PId\":null,\"Children\":[]},{\"Id\":13,\"Name\":\"华盛顿州\",\"PId\":12,\"Children\":[]},{\"Id\":14,\"Name\":\"得克萨斯州\",\"PId\":12,\"Children\":[]}]";
            //FetchToTree: 普通
            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Area>>(json);
            var tree = list.FetchToTree(i => i.Id, i => i.PId, i => i.Children, i => i.PId == null || i.PId == 0).ToList();
            var list2 = new List<string>();
            var res = tree.RecurseTree(i => i.Children, ctx =>
            {
                var depart = ctx.Current;
                int deep = ctx.DeepIndex;
                var parents = ctx.Parents;
                bool b = ctx.IsLeaf;
                list2.Add($"{depart.Name}:{deep}:{b}:{parents.Select(i => i.Name).ToStringSeparated(",")}");
                if (ctx.Current.Name == "湖北") ctx.NextSibling();
            });
            string.Join("\r\n", list2).ShouldBe(@"中国:0:False:
河南:1:False:中国
郑州:2:False:中国,河南
中原区:3:True:中国,河南,郑州
金水区:3:True:中国,河南,郑州
洛阳:2:False:中国,河南
西工区:3:True:中国,河南,洛阳
湖北:1:False:中国
山东:1:True:中国
美国:0:False:
华盛顿州:1:True:美国
得克萨斯州:1:True:美国");
        }
        #endregion
    }
}
