using DotNetCommon.Data;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class ResultTests
    {
        [Test]
        public void Test()
        {
            Result.NotOk("error", code: 20002, data: new { }, extData: "errorDetail");
            Result.OkPage<string>(10, new List<string>() { "xiaoming", "小王" });
        }

        public class GetPersonPageReq : PageQuery
        {
            public string Name { get; set; }
        }
        public class Person { public string Name { get; set; } }

        public ResultPage<Person> GetPersonPage(GetPersonPageReq req)
        {
            //req.PageIndex
            //req.PageSize
            //req.Name
            return Result.OkPage(100, Enumerable.Range(1, 10).Select(i => new Person()).ToList());
        }

        [Test]
        public async Task ResultWrapTest()
        {
            //同步代码块: 捕获异常
            var res = Result.Wrap(() =>
             {
                 int i = 0;
                 i = 5 / i;
             });
            res.Success.ShouldBe(false);
            //异步代码块: 捕获异常
            var res2 = await Result.Wrap(async () =>
            {
                await Task.Run(() =>
                {
                    int i = 0;
                    i = 5 / i;
                });
            });
            res2.Success.ShouldBe(false);

            var res3 = Result.Wrap(() =>
            {
                var i = 0;
            });
            res3.Success.ShouldBe(true);
        }

        [Test]
        public async Task ResultWrapReturnTest()
        {
            //同步代码块: 捕获异常
            var res = Result.WrapReturn(() =>
            {
                int i = 0;
                return 5 / i;
            });
            res.Success.ShouldBe(false);
            //异步代码块: 捕获异常
            var res2 = await Result.WrapReturn(async () =>
            {
                return await Task.Run(() =>
                 {
                     int i = 0;
                     return 5 / i;
                 });
            });
            res2.Success.ShouldBe(false);

            var res3 = await Result.WrapReturn(async () =>
            {
                return await Task.Run(() =>
                {
                    return 0;
                });
            });
            res3.Success.ShouldBe(true);
        }

        [Test]
        public void ResultUnWrapTest()
        {
            var res = Result.Ok(1);
            var i = res.UnWrap();
            i.ShouldBe(1);

            res = Result.NotOk("失败");
            var exp = Should.Throw<Exception>(() =>
              {
                  res.UnWrap();
              });
            exp.Message.ShouldBe("失败");
        }

        [Test]
        public void ReturnTest()
        {
            testMethod();
            Result testMethod()
            {
                // return new Result();
                return Result.NewInstance();
                return Result.Ok().SetData("一些数据").SetCode(100).SetExtData(new { }).SetSuccess().SetFail().SetSuccessFlag(true);
            }
            testMethod2();
            Result<Person> testMethod2()
            {
                return Result.NewInstance<Person>();
                return Result.Ok(new Person()).SetData(new { }).SetCode(100).SetExtData(new { }).SetSuccessFlag(true).SetMessage("");
            }
        }


    }
}
