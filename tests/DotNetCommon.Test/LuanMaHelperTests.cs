﻿using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Test
{
    [TestFixture]
    public class LuanMaHelperTests
    {
        [Test]
        public void Test()
        {
            //System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            LuanMaHelper.InitGBK();
            var str = "测试乱码123";
            var bs_utf8 = Encoding.UTF8.GetBytes(str);
            var bs_gbk = Encoding.GetEncoding("GBK").GetBytes(str);

            //"娴嬭瘯涔辩爜123"
            var str_futf8_tgbk = Encoding.GetEncoding("GBK").GetString(bs_utf8);
            //"��������123"
            var str_fgbk_tutf8 = Encoding.UTF8.GetString(bs_gbk);

            LuanMaHelper.IsLuanMa(str_futf8_tgbk).ShouldBe(false);
            LuanMaHelper.IsLuanMa(str_fgbk_tutf8).ShouldBe(true);


            var finalStr_ansi = LuanMaHelper.GetString(bs_utf8);
            var finalStr_utf8 = LuanMaHelper.GetString(bs_gbk);
            finalStr_ansi.ShouldBe(str);
            finalStr_utf8.ShouldBe(str);


        }
    }
}
