﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Validate;
using Shouldly;
using System.Threading;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class DateTimeValidTests
    {
        [Test]
        public void DateTimeTest()
        {
            var person = new Person();
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
              {
                  ctx.RuleFor(i => i.Birth).MustNotNull().MustGreaterThan(DateTime.Parse("1800-01-01"));
              });
            msg.ShouldContain("'Birth' 必须大于 '1800/1/1 0:00:00'!");

            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Birth).MustNotNull().MustEqualTo(DateTime.Parse("0001-01-01"));
            });
            msg.ShouldBeNullOrEmpty();

            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.CreateTime).MustNotNull();
            });
            msg.ShouldContain("'CreateTime' 不能为空!");

            person.CreateTime = DateTime.Now;
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.CreateTime).MustNotNull();
            });
            msg.ShouldBeNullOrEmpty();

            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.CreateTime).MustLessThanOrEuqalTo(DateTime.Now);
            });
            msg.ShouldBeNullOrEmpty();
            Thread.Sleep(500);
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.CreateTime).MustGreaterThan(DateTime.Now);
            });
            msg.ShouldNotBeNullOrEmpty();

        }

        [Test]
        public void DateTimeOffsetTest()
        {
            var person = new Person();
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Birth2).MustNotNull().MustGreaterThan(DateTime.Parse("1800-01-01"));
            });
            msg.ShouldNotBeNullOrEmpty();

            person.Birth2 = DateTimeOffset.Now;
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Birth2).MustNotNull().MustGreaterThan(DateTime.Parse("1800-01-01"));
            });
            msg.ShouldBeNullOrEmpty();

            person.Birth2 = DateTimeOffset.Parse("0002-01-01");
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Birth2).MustNotNull().MustEqualTo(DateTime.Parse("0002-01-01"));
            });
            msg.ShouldBeNullOrEmpty();

            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Birth2).MustNotNull().MustEqualTo(DateTimeOffset.Parse("0002-01-01"));
            });
            msg.ShouldBeNullOrEmpty();

            person.CreateTime2 = null;
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
              {
                  ctx.RuleFor(i => i.CreateTime2).MustNotNull();
              });
            msg.ShouldNotBeNullOrEmpty();

            person.CreateTime2 = DateTimeOffset.Parse("0003-01-01");
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
             {
                 ctx.RuleFor(i => i.CreateTime2).MustGreaterThan(DateTime.Parse("0002-01-01"));
             });
            msg.ShouldBeNullOrEmpty();
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.CreateTime2).MustGreaterThan(DateTimeOffset.Parse("0002-01-01"));
            });
            msg.ShouldBeNullOrEmpty();

        }

        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public DateTime Birth { get; set; }

            public DateTime? CreateTime { get; set; }

            public DateTimeOffset Birth2 { get; set; }
            public DateTimeOffset? CreateTime2 { get; set; }
        }
    }
}
