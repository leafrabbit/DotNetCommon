﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Validate;
using Shouldly;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class GuidTests
    {
        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                UUID = Guid.NewGuid()
            };
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.UUID).MustNotNull();
            });
            msg.ShouldBeEmpty();
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.UUID).MustBeNull();
            });
            msg.ShouldNotBeEmpty();

            var guid = Guid.NewGuid();
            person.UUID = guid;
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.UUID).MustEqualTo(guid);
            });
            msg.ShouldBeEmpty();
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.UUID).MustLessThan(guid);
            });
            msg.ShouldNotBeEmpty();
        }

        public class Person
        {
            public int Id { get; set; }
            public Guid UUID { get; set; }
            public Guid? UUIDNull { get; set; }
        }
    }
}
