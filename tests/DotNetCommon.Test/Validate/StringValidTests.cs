﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Validate;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class StringValidTests
    {
        [Test]
        public void MustEqualToTest()
        {
            var model = new Model()
            {
                Name = "xiaoming"
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustEqualTo("xiaoming");
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());

            model = new Model()
            {
                Name = "xiaoming2222"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustEqualTo("xiaoming");
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }

        [Test]
        public void NotNullTest()
        {
            var model = new Model()
            {
                Name = "xiaoming"
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustNotNull();
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());

            model = new Model()
            {
                Name = null
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustNotNull();
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }

        /// <summary>
        /// 身份证号校验
        /// </summary>
        [Test]
        public void IsIdCardTest()
        {
            //410926198501022865 格式正确，奇偶校验不通过
            var model = new Model()
            {
                Name = "410926198501022865"
            };
            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustIdCard();
            });
            Assert.True(msg.IsNotNullOrEmptyOrWhiteSpace());
            //410102199003075936 在线生成的
            model = new Model()
            {
                Name = "410102199003075936"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustIdCard();
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());

            //虽然18位,但明显不对
            model = new Model()
            {
                Name = "123456789012345678"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustIdCard();
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }

        [Test]
        public void EmailTest()
        {
            var model = new Model()
            {
                Name = "1286317554@qq.com"
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustEmailAddress();
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());

            model = new Model()
            {
                Name = "retwer.comn.cn45sf"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustEmailAddress();
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }

        [Test]
        public void CellPhoneTest()
        {
            var model = new Model()
            {
                Name = "13526421234"
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustCellPhone();
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());

            model = new Model()
            {
                Name = "45212341223"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustCellPhone();
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());

            model = new Model()
            {
                Name = "+86 15012341234"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustCellPhone();
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());
        }

        [Test]
        public void LengthMustInRangeTest()
        {
            var model = new Model()
            {
                Name = "abcde"
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustLengthInRange(1, 5);
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());

            model = new Model()
            {
                Name = "45212341223"
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.Name).MustLengthInRange(1, 5);
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }


        public class Model
        {
            public string Name { get; set; }
        }
    }
}
