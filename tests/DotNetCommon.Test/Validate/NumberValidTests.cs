﻿using DotNetCommon.Validate;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Extensions;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class NumberValidTests
    {
        [Test]
        public void MustEqualToTest()
        {
            var model = new Model()
            {
                PInt = 1,
                PIntNull = null
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
              {
                  ctx.RuleFor(i => i.PInt).MustEqualTo(1);
                  ctx.RuleFor(i => i.PIntNull).MustEqualTo(null);
              });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());
            model = new Model()
            {
                PFloat = 1.23f,
                PFloatNull = null
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.PFloat).MustEqualTo(1.23f);
                ctx.RuleFor(i => i.PFloatNull).MustEqualTo(null);
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());
        }

        [Test]
        public void MustGreaterThanTest()
        {
            var model = new Model()
            {
                PInt = 1,
                PIntNull = null,
                PFloat = 1.23f,
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.PInt).MustGreaterThan(0);
                ctx.RuleFor(i => i.PFloat).MustGreaterThan(0);
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());
            model = new Model()
            {
                PIntNull = null
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.PIntNull).MustGreaterThan(0);
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }

        [Test]
        public void MustLessThanTest()
        {
            var model = new Model()
            {
                PInt = 1,
                PIntNull = null,
                PFloat = 1.23f,
            };

            var msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.PInt).MustLessThan(2);
                ctx.RuleFor(i => i.PFloat).MustLessThan(2);
            });
            Assert.True(msg.IsNullOrEmptyOrWhiteSpace());
            model = new Model()
            {
                PIntNull = null
            };
            msg = ValidateModelHelper.ValidErrorMessage(model, ctx =>
            {
                ctx.RuleFor(i => i.PIntNull).MustLessThan(0);
            });
            Assert.False(msg.IsNullOrEmptyOrWhiteSpace());
        }


        public class Model
        {
            public short PShort { get; set; }
            public short? PShortNull { get; set; }
            public int PInt { get; set; }
            public int? PIntNull { get; set; }
            public long PLong { get; set; }
            public long? PLongNull { get; set; }
            public float PFloat { get; set; }
            public float? PFloatNull { get; set; }
            public double PDouble { get; set; }
            public double? PDoubleNull { get; set; }
            public decimal PDecimal { get; set; }
            public decimal? PDecimalNull { get; set; }


            public ushort PUShort { get; set; }
            public ushort? PUShortNull { get; set; }
            public uint PUInt { get; set; }
            public uint? PUIntNull { get; set; }
            public ulong PULong { get; set; }
            public ulong? PULongNull { get; set; }
        }

    }
}
