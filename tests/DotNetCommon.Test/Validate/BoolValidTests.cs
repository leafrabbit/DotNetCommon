﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using DotNetCommon.Validate;
using Shouldly;
using System.Diagnostics;

namespace DotNetCommon.Test.Validate
{
    [TestFixture]
    public class BoolValidTests
    {
        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                Active = true,
                Hire = false
            };
            var msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
              {
                  ctx.RuleFor(i => i.Id).MustGreaterThanZero();
                  ctx.RuleFor(i => i.Active).MustEqualTo(true);
                  ctx.RuleFor(i => i.Hire).MustEqualTo(false);
              });
            msg.ShouldBeEmpty();

            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Active).MustBeTrue();
                ctx.RuleFor(i => i.Hire).MustBeFalse();
            });
            msg.ShouldBeEmpty();

            //'Active' 必须为false!
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Active).MustBeFalse();
                ctx.RuleFor(i => i.Hire).MustBeFalse();
            });
            msg.ShouldContain("'Active' 必须为false!");

            //'Active' 必须为空!
            msg = ValidateModelHelper.ValidErrorMessage(person, ctx =>
            {
                ctx.RuleFor(i => i.Active).MustBeNull();
                ctx.RuleFor(i => i.Hire).MustNotNull();
            });
            msg.ShouldContain("'Active' 必须为空!");
        }

        public class Person
        {
            public int Id { get; set; }
            public bool Active { get; set; }
            public bool? Hire { get; set; }
        }
    }
}
