﻿namespace DotNetCommon.Test.EasyComparer
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using DotNetCommon.EasyComparer;
    using NUnit.Framework;
    using Shouldly;

    [TestFixture]
    internal sealed class EasyComparerTests
    {
        [Test]
        public void When_getting_instance_multiple_times()
        {
            var one = EasyComparer.Instance;
            var two = EasyComparer.Instance;
            one.ShouldBeSameAs(two);
        }

        [Test]
        public void When_comparing_a_reference_object_to_itself_including_base_excluding_privates()
        {
            var obj = new SomeClass();

            EasyComparer.Instance.Compare(obj, obj, true, false, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.Count.ShouldBe(12);
            result.ShouldAllBe(v => v.Value.Varies == false);

            result.ShouldContain(p => p.Key.Name == "Id");
            result.ShouldContain(p => p.Key.Name == "Age");
            result.ShouldContain(p => p.Key.Name == "Stopwatch");
            result.ShouldContain(p => p.Key.Name == "Bytes");
            result.ShouldContain(p => p.Key.Name == "Name");
            result.ShouldContain(p => p.Key.Name == "SomeArray");
            result.ShouldContain(p => p.Key.Name == "SomeList");
            result.ShouldContain(p => p.Key.Name == "SomeCollection");
            result.ShouldContain(p => p.Key.Name == "SomeEnumerable");
            result.ShouldContain(p => p.Key.Name == "SomeDictionary");
            result.ShouldContain(p => p.Key.Name == "SomeNullable");
            result.ShouldContain(p => p.Key.Name == "SomeDate");

            result.ShouldNotContain(p => p.Key.Name == "SomePrivate");
            result.ShouldNotContain(p => p.Key.Name == "SomeInternal");
        }

        [Test]
        public void When_comparing_a_reference_object_to_itself_including_base_including_privates()
        {
            var obj = new SomeClass();

            EasyComparer.Instance.Compare(obj, obj, true, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(14);
            result.ShouldAllBe(v => v.Value.Varies == false);

            result.ShouldContain(p => p.Key.Name == "Id");
            result.ShouldContain(p => p.Key.Name == "Age");
            result.ShouldContain(p => p.Key.Name == "Stopwatch");
            result.ShouldContain(p => p.Key.Name == "Bytes");
            result.ShouldContain(p => p.Key.Name == "Name");
            result.ShouldContain(p => p.Key.Name == "SomeArray");
            result.ShouldContain(p => p.Key.Name == "SomeList");
            result.ShouldContain(p => p.Key.Name == "SomeCollection");
            result.ShouldContain(p => p.Key.Name == "SomeEnumerable");
            result.ShouldContain(p => p.Key.Name == "SomeDictionary");
            result.ShouldContain(p => p.Key.Name == "SomeNullable");
            result.ShouldContain(p => p.Key.Name == "SomeDate");
            result.ShouldContain(p => p.Key.Name == "SomePrivate");
            result.ShouldContain(p => p.Key.Name == "SomeInternal");
        }

        [Test]
        public void When_comparing_a_reference_object_to_itself_excluding_base_including_privates()
        {
            var obj = new SomeClass();

            EasyComparer.Instance.Compare(obj, obj, false, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(13);
            result.ShouldAllBe(v => v.Value.Varies == false);

            result.ShouldContain(p => p.Key.Name == "Age");
            result.ShouldContain(p => p.Key.Name == "Stopwatch");
            result.ShouldContain(p => p.Key.Name == "Bytes");
            result.ShouldContain(p => p.Key.Name == "Name");
            result.ShouldContain(p => p.Key.Name == "SomeArray");
            result.ShouldContain(p => p.Key.Name == "SomeList");
            result.ShouldContain(p => p.Key.Name == "SomeCollection");
            result.ShouldContain(p => p.Key.Name == "SomeEnumerable");
            result.ShouldContain(p => p.Key.Name == "SomeDictionary");
            result.ShouldContain(p => p.Key.Name == "SomeNullable");
            result.ShouldContain(p => p.Key.Name == "SomeDate");
            result.ShouldContain(p => p.Key.Name == "SomePrivate");
            result.ShouldContain(p => p.Key.Name == "SomeInternal");
        }

        [Test]
        public void When_comparing_equal_reference_objects_including_base_excluding_privates()
        {
            var left = new SomeClass
            {
                Id = 6,
                Age = 1,
                Stopwatch = new Stopwatch(),
                Bytes = new byte[] { 1, 2, 0 },
                Name = "Foo",
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" },
                SomeNullable = 42,
                SomeDate = DateTime.UtcNow,
                SomeInternal = 1_234_567
            };

            var right = new SomeClass
            {
                Id = left.Id,
                Age = left.Age,
                Stopwatch = left.Stopwatch,
                Bytes = left.Bytes,
                Name = left.Name,
                SomeArray = left.SomeArray,
                SomeList = left.SomeList,
                SomeCollection = left.SomeCollection,
                SomeEnumerable = left.SomeEnumerable,
                SomeDictionary = left.SomeDictionary,
                SomeNullable = left.SomeNullable,
                SomeDate = left.SomeDate,
                SomeInternal = left.SomeInternal
            };

            EasyComparer.Instance.Compare(left, right, true, false, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(12);
            result.ShouldAllBe(v => v.Value.Varies == false);
        }

        [Test]
        public void When_comparing_equal_reference_objects_including_base_including_privates()
        {
            var left = new SomeClass
            {
                Id = 6,
                Age = 1,
                Stopwatch = new Stopwatch(),
                Bytes = new byte[] { 1, 2, 0 },
                Name = "Foo",
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" },
                SomeNullable = 42,
                SomeDate = DateTime.UtcNow,
                SomeInternal = 1_234_567
            };

            var right = new SomeClass
            {
                Id = left.Id,
                Age = left.Age,
                Stopwatch = left.Stopwatch,
                Bytes = left.Bytes,
                Name = left.Name,
                SomeArray = left.SomeArray,
                SomeList = left.SomeList,
                SomeCollection = left.SomeCollection,
                SomeEnumerable = left.SomeEnumerable,
                SomeDictionary = left.SomeDictionary,
                SomeNullable = left.SomeNullable,
                SomeDate = left.SomeDate,
                SomeInternal = left.SomeInternal
            };

            EasyComparer.Instance.Compare(left, right, true, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(14);
            result.ShouldAllBe(v => v.Value.Varies == false);
        }

        [Test]
        public void When_comparing_equal_reference_objects_with_different_collections_but_same_underlying_values()
        {
            var left = new SomeClass
            {
                Id = 6,
                Bytes = new byte[] { 1, 2, 0 },
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" }
            };

            var right = new SomeClass
            {
                Id = 6,
                Bytes = new byte[] { 1, 2, 0 },
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" }
            };

            EasyComparer.Instance.Compare(left, right, true, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(14);
            result.ShouldAllBe(v => v.Value.Varies == false);
        }

        [Test]
        public void When_comparing_equal_reference_objects_with_different_collections()
        {
            var left = new SomeClass
            {
                Bytes = new byte[] { 1, 2, 0 },
            };

            var right = new SomeClass
            {
                Bytes = new byte[] { 0, 1, 2 },
            };

            EasyComparer.Instance.Compare(left, right, true, true, out var result)
                .ShouldBeFalse();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(14);
            result.Count(v => v.Value.Varies == false)
                .ShouldBe(13);

            var varriedProperty = result.Single(p => p.Key.Name == "Bytes");
            varriedProperty.Value.Varies.ShouldBeTrue();
            varriedProperty.Value.LeftValue.ShouldBe(new byte[] { 1, 2, 0 });
            varriedProperty.Value.RightValue.ShouldBe(new byte[] { 0, 1, 2 });
        }

        [Test]
        public void When_comparing_a_struct_object_to_itself_including_base_excluding_privates()
        {
            var obj = new SomeStruct();

            EasyComparer.Instance.Compare(obj, obj, true, false, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(11);
            result.ShouldAllBe(v => v.Value.Varies == false);

            result.ShouldContain(p => p.Key.Name == "Age");
            result.ShouldContain(p => p.Key.Name == "Stopwatch");
            result.ShouldContain(p => p.Key.Name == "Bytes");
            result.ShouldContain(p => p.Key.Name == "Name");
            result.ShouldContain(p => p.Key.Name == "SomeArray");
            result.ShouldContain(p => p.Key.Name == "SomeList");
            result.ShouldContain(p => p.Key.Name == "SomeCollection");
            result.ShouldContain(p => p.Key.Name == "SomeEnumerable");
            result.ShouldContain(p => p.Key.Name == "SomeDictionary");
            result.ShouldContain(p => p.Key.Name == "SomeNullable");
            result.ShouldContain(p => p.Key.Name == "SomeDate");

            result.ShouldNotContain(p => p.Key.Name == "SomePrivate");
            result.ShouldNotContain(p => p.Key.Name == "SomeInternal");
        }

        [Test]
        public void When_comparing_a_struct_object_to_itself_including_base_including_privates()
        {
            var obj = new SomeStruct();

            EasyComparer.Instance.Compare(obj, obj, true, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(13);
            result.ShouldAllBe(v => v.Value.Varies == false);

            result.ShouldContain(p => p.Key.Name == "Age");
            result.ShouldContain(p => p.Key.Name == "Stopwatch");
            result.ShouldContain(p => p.Key.Name == "Bytes");
            result.ShouldContain(p => p.Key.Name == "Name");
            result.ShouldContain(p => p.Key.Name == "SomeArray");
            result.ShouldContain(p => p.Key.Name == "SomeList");
            result.ShouldContain(p => p.Key.Name == "SomeCollection");
            result.ShouldContain(p => p.Key.Name == "SomeEnumerable");
            result.ShouldContain(p => p.Key.Name == "SomeDictionary");
            result.ShouldContain(p => p.Key.Name == "SomeNullable");
            result.ShouldContain(p => p.Key.Name == "SomeDate");
            result.ShouldContain(p => p.Key.Name == "SomePrivate");
            result.ShouldContain(p => p.Key.Name == "SomeInternal");
        }

        [Test]
        public void When_comparing_a_struct_object_to_itself_excluding_base_including_privates()
        {
            var obj = new SomeStruct();

            EasyComparer.Instance.Compare(obj, obj, false, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(13);
            result.ShouldAllBe(v => v.Value.Varies == false);

            result.ShouldContain(p => p.Key.Name == "Age");
            result.ShouldContain(p => p.Key.Name == "Stopwatch");
            result.ShouldContain(p => p.Key.Name == "Bytes");
            result.ShouldContain(p => p.Key.Name == "Name");
            result.ShouldContain(p => p.Key.Name == "SomeArray");
            result.ShouldContain(p => p.Key.Name == "SomeList");
            result.ShouldContain(p => p.Key.Name == "SomeCollection");
            result.ShouldContain(p => p.Key.Name == "SomeEnumerable");
            result.ShouldContain(p => p.Key.Name == "SomeDictionary");
            result.ShouldContain(p => p.Key.Name == "SomeNullable");
            result.ShouldContain(p => p.Key.Name == "SomeDate");
            result.ShouldContain(p => p.Key.Name == "SomePrivate");
            result.ShouldContain(p => p.Key.Name == "SomeInternal");
        }

        [Test]
        public void When_comparing_equal_struct_objects_including_base_excluding_privates()
        {
            var left = new SomeStruct
            {
                Age = 1,
                Stopwatch = new Stopwatch(),
                Bytes = new byte[] { 1, 2, 0 },
                Name = "Foo",
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" },
                SomeNullable = 42,
                SomeDate = DateTime.UtcNow,
                SomeInternal = 1_234_567
            };

            var right = new SomeStruct
            {
                Age = left.Age,
                Stopwatch = left.Stopwatch,
                Bytes = left.Bytes,
                Name = left.Name,
                SomeArray = left.SomeArray,
                SomeList = left.SomeList,
                SomeCollection = left.SomeCollection,
                SomeEnumerable = left.SomeEnumerable,
                SomeDictionary = left.SomeDictionary,
                SomeNullable = left.SomeNullable,
                SomeDate = left.SomeDate,
                SomeInternal = left.SomeInternal
            };

            EasyComparer.Instance.Compare(left, right, true, false, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(11);
            result.ShouldAllBe(v => v.Value.Varies == false);
        }

        [Test]
        public void When_comparing_equal_struct_objects_including_base_including_privates()
        {
            var left = new SomeStruct
            {
                Age = 1,
                Stopwatch = new Stopwatch(),
                Bytes = new byte[] { 1, 2, 0 },
                Name = "Foo",
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" },
                SomeNullable = 42,
                SomeDate = DateTime.UtcNow,
                SomeInternal = 1_234_567
            };

            var right = new SomeStruct
            {
                Age = left.Age,
                Stopwatch = left.Stopwatch,
                Bytes = left.Bytes,
                Name = left.Name,
                SomeArray = left.SomeArray,
                SomeList = left.SomeList,
                SomeCollection = left.SomeCollection,
                SomeEnumerable = left.SomeEnumerable,
                SomeDictionary = left.SomeDictionary,
                SomeNullable = left.SomeNullable,
                SomeDate = left.SomeDate,
                SomeInternal = left.SomeInternal
            };

            EasyComparer.Instance.Compare(left, right, true, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(13);
            result.ShouldAllBe(v => v.Value.Varies == false);
        }

        [Test]
        public void When_comparing_equal_struct_objects_with_different_collections_but_same_underlying_values()
        {
            var left = new SomeStruct
            {
                Bytes = new byte[] { 1, 2, 0 },
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" }
            };

            var right = new SomeStruct
            {
                Bytes = new byte[] { 1, 2, 0 },
                SomeArray = new[] { 1, 2, 3 },
                SomeList = new List<int> { 7, 8, 9 },
                SomeCollection = new Collection<int> { 4, 5, 6 },
                SomeEnumerable = Enumerable.Range(1, 4),
                SomeDictionary = new Dictionary<int, string> { [0] = "Bar" }
            };

            EasyComparer.Instance.Compare(left, right, true, true, out var result)
                .ShouldBeTrue();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(13);
            result.ShouldAllBe(v => v.Value.Varies == false);
        }

        [Test]
        public void When_comparing_equal_struct_objects_with_different_collections()
        {
            var left = new SomeStruct
            {
                Bytes = new byte[] { 1, 2, 0 },
            };

            var right = new SomeStruct
            {
                Bytes = new byte[] { 0, 1, 2 },
            };

            EasyComparer.Instance.Compare(left, right, true, true, out var result)
                .ShouldBeFalse();

            result.ShouldNotBeNull();
            result.ShouldNotBeEmpty();
            result.Count.ShouldBe(13);
            result.Count(v => v.Value.Varies == false)
                .ShouldBe(12);

            var varriedProperty = result.Single(p => p.Key.Name == "Bytes");
            varriedProperty.Value.Varies.ShouldBeTrue();
            varriedProperty.Value.LeftValue.ShouldBe(new byte[] { 1, 2, 0 });
            varriedProperty.Value.RightValue.ShouldBe(new byte[] { 0, 1, 2 });
        }

        [Test]
        public void Test2()
        {
            var left = new SomeClass()
            {
                Id = 1,
                Age = 18,
                Bytes = new byte[10],
                Name = "小明",
                SomeArray = new int[2] { 1, 2 },
                SomeCollection = new List<int>() { 1, 2 },
                SomeDate = DateTime.Parse("2020-01-01"),
                SomeDictionary = new Dictionary<int, string>() { { 1, "小明" }, { 2, "小红" } },
                SomeEnumerable = new int[] { 1, 2 },
                SomeInternal = 100L,
                SomeList = new List<int>() { 1, 2 },
                SomeNullable = null,
                Stopwatch = new Stopwatch()
            };
            var right = new SomeClass()
            {
                Id = 1,
                Age = 18,
                Bytes = new byte[5],
                Name = "小明",
                SomeArray = new int[2] { 2, 1 },
                SomeCollection = new List<int>() { 200, 2 },
                SomeDate = DateTime.Parse("2020-01-01"),
                SomeDictionary = new Dictionary<int, string>() { { 1, "小明" }, { 2, "小红" } },
                SomeEnumerable = new int[] { 1, 2 },
                SomeInternal = 100L,
                SomeList = new List<int>() { 1, 2 },
                SomeNullable = null,
                Stopwatch = new Stopwatch()
            };
            var isEqual = EasyComparer.Instance.Compare(left, right, true, true, out var results);
            if (!isEqual)
            {
                foreach (var item in results)
                {
                    Console.WriteLine($"属性:{item.Key.Name},是否有变化:{item.Value.Varies}");
                }
            }
            //输出：
            /*
            属性:Age,是否有变化:False
            属性:Stopwatch,是否有变化:True
            属性:Bytes,是否有变化:True
            属性:Name,是否有变化:False
            属性:SomeArray,是否有变化:True
            属性:SomeList,是否有变化:False
            属性:SomeCollection,是否有变化:True
            属性:SomeEnumerable,是否有变化:False
            属性:SomeDictionary,是否有变化:False
            属性:SomeNullable,是否有变化:False
            属性:SomeDate,是否有变化:False
            属性:SomePrivate,是否有变化:False
            属性:SomeInternal,是否有变化:False
            属性:Id,是否有变化:False
            */
        }

        private class SomeBase
        {
            public short Id { get; set; }
        }

        private sealed class SomeClass : SomeBase
        {
            public int Age { get; set; }
            public Stopwatch Stopwatch { get; set; }
            public byte[] Bytes { get; set; }
            public string Name { get; set; }
            public int[] SomeArray { get; set; }
            public IList<int> SomeList { get; set; }
            public ICollection<int> SomeCollection { get; set; }
            public IEnumerable<int> SomeEnumerable { get; set; }
            public IDictionary<int, string> SomeDictionary { get; set; }
            public int? SomeNullable { get; set; }
            public DateTime SomeDate { get; set; }
            private uint SomePrivate { get; set; }
            internal long SomeInternal { get; set; }
        }

        private struct SomeStruct
        {
            public int Age { get; set; }
            public Stopwatch Stopwatch { get; set; }
            public byte[] Bytes { get; set; }
            public string Name { get; set; }
            public int[] SomeArray { get; set; }
            public IList<int> SomeList { get; set; }
            public ICollection<int> SomeCollection { get; set; }
            public IEnumerable<int> SomeEnumerable { get; set; }
            public IDictionary<int, string> SomeDictionary { get; set; }
            public int? SomeNullable { get; set; }
            public DateTime SomeDate { get; set; }
            private uint SomePrivate { get; set; }
            internal long SomeInternal { get; set; }
        }

        [Test]
        public void Test()
        {
            var person = new Person()
            {
                Id = 1,
                Name = "小明"
            };
            var person2 = new Person()
            {
                Id = 1,
                Name = "小红"
            };
            //比对两个对象,并指定包含继承的属性和私有属性
            var usVary = EasyComparer.Instance.Compare(person, person2, true, true, out var results);
            foreach (var item in results)
            {
                Console.WriteLine($"属性:{item.Key.Name},是否有变化:{item.Value.Varies}");
            }
            /*输出比较结果如下：
            属性:Id,是否有变化:False
            属性:Name,是否有变化:True
            */
        }

        public class Person
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        [Test]
        public void Test3()
        {
            var left = new Outer()
            {
                Id = 1,
                Name = "小明",
                InnerModel = new Inner()
                {
                    Id = 2,
                    Name = "小红"
                }
            };

            var right = new Outer()
            {
                Id = 1,
                Name = "小明",
                InnerModel = new Inner()
                {
                    Id = 2,
                    Name = "小红"
                }
            };

            if (!EasyComparer.Instance.Compare(left, right, true, true, out var results))
            {
                foreach (var item in results)
                {
                    Console.WriteLine($"属性:{item.Key.Name},是否有变化:{item.Value.Varies}");
                }
            }
            else
            {
                Console.WriteLine($"对象无变化");
            }
            //输出:
            /*
            属性:Id,是否有变化:False
            属性:Name,是否有变化:False
            属性:InnerModel,是否有变化:True
            */
        }

        public class Outer
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public Inner InnerModel { get; set; }
        }

        public class Inner
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}