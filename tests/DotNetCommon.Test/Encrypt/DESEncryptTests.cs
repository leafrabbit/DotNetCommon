﻿using DotNetCommon.Encrypt;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCommon.Tests.Encrypt
{
    [TestFixture]
    public class DESEncryptTests
    {
        [Test]
        public void EncryptTest()
        {
            var sensitiveData = "机密数据";
            var key = "12345678";
            //DES加解密
            //87F0CBDF0C138C8C45E2DEF3F04EE5DF
            var res = DESEncrypt.Encrypt(sensitiveData, key);
            var str2 = DESEncrypt.Decrypt(res, key);

            sensitiveData = "";
            //加密后: ""
            res = DESEncrypt.Encrypt(sensitiveData, key);
            str2 = DESEncrypt.Decrypt(res, key);

            sensitiveData = null;
            //加密后: null
            res = DESEncrypt.Encrypt(sensitiveData, key);
            str2 = DESEncrypt.Decrypt(res, key);

            sensitiveData = " ";
            //加密后: ACCD6295AFF716BEAEDD1F879F57444D
            res = DESEncrypt.Encrypt(sensitiveData, key);
            str2 = DESEncrypt.Decrypt(res, key);
        }

        /// <summary>
        /// 秘钥的长度必须为8位
        /// </summary>
        [Test]
        public void EncryptTest2()
        {
            var sensitiveData = "机密数据";
            var key = "1234567890";
            //DES加解密
            Should.Throw<Exception>(() => DESEncrypt.Encrypt(sensitiveData, key));
            Should.Throw<Exception>(() => DESEncrypt.Decrypt("HUSDJSIND", key));
        }


        [Test]
        public void EncryptStream()
        {
            var sensitiveData = "机密数据";
            var memostream = new System.IO.MemoryStream();
            var bs = Encoding.UTF8.GetBytes(sensitiveData);
            memostream.Write(bs, 0, bs.Length);
            var key = "12345678";
            //DES加解密流
            var temppath = DESEncrypt.Encrypt(memostream, key);
            var bs2 = File.ReadAllBytes(temppath);
            var str = Encoding.UTF8.GetString(bs2);
            str.ShouldNotBe(sensitiveData);
            File.Delete(temppath);


            temppath = DESEncrypt.Decrypt(new MemoryStream(bs2), key);
            bs2 = File.ReadAllBytes(temppath);
            str = Encoding.UTF8.GetString(bs2);
            str.ShouldBe(sensitiveData);
            File.Delete(temppath);

            //使用自定义的临时文件目录
            //DES加解密流
            temppath = DESEncrypt.Encrypt(memostream, key, AppDomain.CurrentDomain.BaseDirectory);
            bs2 = File.ReadAllBytes(temppath);
            str = Encoding.UTF8.GetString(bs2);
            str.ShouldNotBe(sensitiveData);
            File.Delete(temppath);


            temppath = DESEncrypt.Decrypt(new MemoryStream(bs2), key, AppDomain.CurrentDomain.BaseDirectory);
            bs2 = File.ReadAllBytes(temppath);
            str = Encoding.UTF8.GetString(bs2);
            str.ShouldBe(sensitiveData);
            File.Delete(temppath);
        }
    }
}
