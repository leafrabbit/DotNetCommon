﻿using DotNetCommon.Encrypt;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCommon.Tests.Encrypt
{
    [TestFixture]
    public class MD5EncryptTests
    {
        [Test]
        public void EncodeTest()
        {
            //C3FCD3D76192E4007DFB496CCA67E13B
            var res = MD5Encrypt.MD5("abcdefghijklmnopqrstuvwxyz");
            res.ShouldNotBeNullOrWhiteSpace();
            var res2 = MD5Encrypt.MD5("abcdefghijklmnopqrstuvwxy");
            res2.ShouldNotBeNullOrWhiteSpace();
            Assert.AreNotEqual(res, res2);

            res = MD5Encrypt.MD5("");
            res.ShouldBe("");

            string str = null;
            res = MD5Encrypt.MD5(str);
            res.ShouldBe(null);
        }

        [Test]
        public void EncodeStreamTest()
        {
            var memo1 = new MemoryStream(Encoding.UTF8.GetBytes("小明你好1"));
            var memo2 = new MemoryStream(Encoding.UTF8.GetBytes("小明你好2"));
            var res = MD5Encrypt.MD5(memo1);
            var res2 = MD5Encrypt.MD5(memo2);
            Assert.AreNotEqual(res, res2);

            Stream stream = null;
            Should.Throw<Exception>(() => MD5Encrypt.MD5(stream));
        }

        [Test]
        public void EncodeFileTest()
        {
            var file1 = Path.GetFullPath("Encrypt/YXSPublicKey.cer");
            var file2 = Path.GetFullPath("Encrypt/YXSPrivateKey.pfx");
            var res = MD5Encrypt.MD5File(file1);
            var res2 = MD5Encrypt.MD5File(file2);
            Assert.AreNotEqual(res, res2);

            res = MD5Encrypt.MD5File(null);
            res.ShouldBe(null);
            res = MD5Encrypt.MD5File("");
            res.ShouldBe("");
            res = MD5Encrypt.MD5File("  ");
            res.ShouldBe("  ");
        }
    }
}
