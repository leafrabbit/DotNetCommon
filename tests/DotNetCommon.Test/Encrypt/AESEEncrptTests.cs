﻿using DotNetCommon.Encrypt;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DotNetCommon.Test.Encrypt
{
    [TestFixture]
    public class AESEEncrptTests
    {
        [Test]
        public void TestKey()
        {
            var sensitiveData = "机密数据";
            var key33 = "123456789012345678901234567890123";
            //AES加解密
            //8BBD9948261229F54079463D46A88344
            Shouldly.Should.Throw<Exception>(() => AESEncrypt.Encrypt(sensitiveData, key33));
            Shouldly.Should.Throw<Exception>(() => AESEncrypt.Decrypt(sensitiveData, key33));

            Shouldly.Should.Throw<Exception>(() => AESEncrypt.Encrypt(sensitiveData, null));
            Shouldly.Should.Throw<Exception>(() => AESEncrypt.Decrypt(sensitiveData, null));

            Shouldly.Should.Throw<Exception>(() => AESEncrypt.Encrypt(sensitiveData, ""));
            Shouldly.Should.Throw<Exception>(() => AESEncrypt.Decrypt(sensitiveData, ""));
        }

        [Test]
        public void TestString()
        {
            var sensitiveData = "机密数据";
            var key = "12345678";
            //AES加解密
            //DF3E754E3B903956C562B79AA63C431E
            var res2 = AESEncrypt.Encrypt(sensitiveData, key);
            var str3 = AESEncrypt.Decrypt(res2, key);

            sensitiveData = "";
            //加密后: ""
            res2 = AESEncrypt.Encrypt(sensitiveData, key);
            str3 = AESEncrypt.Decrypt(res2, key);

            sensitiveData = null;
            //加密后: null
            res2 = AESEncrypt.Encrypt(sensitiveData, key);
            str3 = AESEncrypt.Decrypt(res2, key);

            sensitiveData = " ";
            //加密后: 7FB4DB1A900E860CF25285B6E407C300
            res2 = AESEncrypt.Encrypt(sensitiveData, key);
            str3 = AESEncrypt.Decrypt(res2, key);
        }

        [Test]
        public void EncryptStream()
        {
            var sensitiveData = "机密数据";
            var memostream = new System.IO.MemoryStream();
            var bs = Encoding.UTF8.GetBytes(sensitiveData);
            memostream.Write(bs, 0, bs.Length);
            var key = "12345678";
            //DES加解密流
            var temppath = AESEncrypt.Encrypt(memostream, key);
            var bs2 = File.ReadAllBytes(temppath);
            var str = Encoding.UTF8.GetString(bs2);
            str.ShouldNotBe(sensitiveData);
            File.Delete(temppath);


            temppath = AESEncrypt.Decrypt(new MemoryStream(bs2), key);
            bs2 = File.ReadAllBytes(temppath);
            str = Encoding.UTF8.GetString(bs2);
            str.ShouldBe(sensitiveData);
            File.Delete(temppath);

            //使用自定义的临时文件目录
            //DES加解密流
            temppath = AESEncrypt.Encrypt(memostream, key, AppDomain.CurrentDomain.BaseDirectory);
            bs2 = File.ReadAllBytes(temppath);
            str = Encoding.UTF8.GetString(bs2);
            str.ShouldNotBe(sensitiveData);
            File.Delete(temppath);


            temppath = AESEncrypt.Decrypt(new MemoryStream(bs2), key, AppDomain.CurrentDomain.BaseDirectory);
            bs2 = File.ReadAllBytes(temppath);
            str = Encoding.UTF8.GetString(bs2);
            str.ShouldBe(sensitiveData);
            File.Delete(temppath);
        }
    }
}
