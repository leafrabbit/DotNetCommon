# DotNetCommon

#### 介绍
搜集.neter开发常用的功能，运行环境： .net4.7；.netstandard2.0；net5.0；net6.0；

[![nuget](https://img.shields.io/nuget/v/DotNetCommon.svg?style=flat-square)](https://www.nuget.org/packages/DotNetCommon) [![stats](https://img.shields.io/nuget/dt/DotNetCommon.svg?style=flat-square)](https://www.nuget.org/stats/packages/DotNetCommon?groupby=Version) [![GitHub license](https://img.shields.io/badge/license-MIT-green)](https://gitee.com/jackletter/DotNetCommon/blob/master/LICENSE)



**注意： 从DotNetCommon 2.1.0之后，独立出来 DotNetCommon.Core包、DotNetCommon.PinYin包，目的是为了减少核心包体积，如下图：**

![image-20210827115634741](docs/imgs/image-20210827115634741.png)

## 功能列表
1. 通用数据模型；
2. 通用树状结构&平铺数据的访问；
3. 通用身份认证模型；
4. 通用数据类型转换之Object.To方法；
5. 通用Dto间转换之Object.Mapper扩展；
6. 递归篡改对象的属性值之Modify扩展；
7. 将Dto属性投影到Entity之ModifyByDto扩展；
8. 校验框架;
9. 分布式id&分布式流水号；
10. 编码和加解密；
11. 序列化；
12. 汉字转拼音；
13. 压缩&解压缩；
14. 注册表；
15. 验证码生成;
16. 随机数；
17. 对象池；
18. 基于内存的并发消息队列；
19. 反射工具；
20. 主机诊断报告；
21. 对象深度比对工具；
22. 网络帮助类；
23. 单位转换器（B/KB/MS/GB）；
24. 金额大小写转换；
25. 枚举类型扩展方法；
26. 常用扩展方法；
27. 中文乱码检测（GBK or UTF-8）；

## 更多功能介绍

查看：https://gitee.com/jackletter/DotNetCommon/tree/master/docs



## 快速开始

### 1.安装包

```
dotnet add package DotNetCommon --version 2.1.4
```
### 2. 引入命名空间

```csharp
using DotNetCommon;
using DotNetCommon.Extensions;
```
### 3. 功能示例
#### 3.1 数据模型

```csharp
public Result<Person> GetUserById(int id)
{
    if (id < 0) return Result.NotOk("id必须大于0!");
    //...
    return Result.Ok(new Person());
}
```
#### 3.2 加解密
```csharp
public void EncryptTest()
{
    var sensitiveData = "敏感信息";
    var key = "12345678"; 
    //加密
    var res = DESEncrypt.Encrypt(sensitiveData, key);
    //解密
	var res2= DESEncrypt.Decrypt(res, key);
}
```

#### 3.3 分布式Id & 分布式流水号
```csharp
public void Test()
{
    //首先设置当前机器id: 0-1023
    Machine.SetMachineId(1);
    //生成分布式id
    //输出示例: 185081270290616320
    long id = DistributeGenerator.NewId("key");
    //生成分布式流水号
    //输出示例: sno202105250001000001
    string sno = DistributeGenerator.NewSNO("sno", SerialFormat.CreateDistributeFast("sno", "yyyyMMdd", 6));
}
```

#### 3.4 序列化
```csharp
/// <summary>
/// 指定常用的设置,序列化为json字符串
/// </summary>
/// <param name="obj"></param>
/// <param name="dateFormatString">日期时间格式</param>
/// <param name="isLongToString">是否将long型转为字符串</param>
/// <param name="IgnoreNull">是否忽略null值的属性</param>
/// <param name="enum2String">是否将枚举转换为字符串</param>
/// <param name="lowerCamelCase">属性名称的首字母是否小写</param>
/// <param name="isIntend">是否格式缩进</param>
/// <param name="isAllToString">是否将所有数据类型转换为字符串</param>
/// <param name="allNumDigit">设定的所有数字的最大小数位数(默认为null，即: 不限制)</param>
/// <param name="decimalDigit">仅针对decimal设定的最大小数位数(默认为null，即: 不限制)</param>
/// <param name="otherSettings">其他的设置</param>
/// <returns></returns>
public static string ToJsonFast(this object obj, string dateFormatString = "yyyy-MM-dd HH:mm:ss", bool IgnoreNull = false, bool enum2String = true, bool lowerCamelCase = false, bool isIntend = false, bool isLongToString = false, bool isAllToString = false, int? allNumDigit = null, int? decimalDigit = null, Action<JsonSerializerSettings> otherSettings = null)
```
#### 3.5 压缩&解压缩
```csharp
//压缩单个文件
ZipHelper.ZipFile("c:\\tmp.zip","d:\\test.txt");
//压缩多个文件
ZipHelper.ZipFile("c:\\tmp.zip","d:\\test.txt"，"d:\\test2.txt");
//压缩单个目录
ZipHelper.ZipFolder("c:\\tmp.zip","d:\\test1");
//压缩多个目录
ZipHelper.ZipFolder("c:\\tmp.zip","d:\\test1"，"d:\\test2");

//压缩多个文件，并为每个文件指定名称
ZipHelper.ZipFile("c:\\tmp.zip",
                ("c:\\testfolder-中文B.txt", "重命名1.txt"),
                ("c:\\testsubfolder-suba.txt", "\\sub\\重命名2.txt"))
```

#### 3.6 类似AutoMapper的转换
```csharp
public class Cat
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public DateTime Birth { get; set; }
}

public class CatDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age
    {
        get
        {
            return DateTime.Now.Year - Birth.Year;
        }
    }
    public DateTime Birth { get; set; }
}

//转换示例
var cat = new Cat()
{
    Id = 1,
    Name = "小明",
    Birth = DateTime.Parse("1989-01-02"),
    Age = 20
};
var dto = cat.Mapper<CatDto>();
dto.ShouldNotBeNull();
dto.Id.ShouldBe(1);
dto.Name.ShouldBe("小明");
dto.Age.ShouldNotBe(20);
```



#### 3.7 类FluentValidation校验组件

```csharp
//Service层方法，添加实体
public Result<bool> AddStudent(Student student)
{
    var res = ValidateModelHelper.ValidResult(student, Student.ValidAdd);
    if (!res.Success) return res;
    //...新增操作
    return Result.Ok(true);
}

public class Student
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int? Age { get; set; }
    public DateTime? Birth { get; set; }
    public string IdCard { get; set; }
    public string Addr { get; set; }
    public string Phone { get; set; }
    public string Email { get; set; }

    /// <summary>
    /// 校验新增Student
    /// </summary>
    /// <param name="ctx"></param>
    public static void ValidAdd(ValidateContext<Student> ctx)
    {
        //请求实体不能为null，否则直接中断校验
        ctx.MustNotNull().IfFailThenExit();
        //Id必须为0
        ctx.RuleFor(i => i.Id).MustEqualTo(0);
        //姓名不能为空且长度在1-4之间
        ctx.RuleFor(i => i.Name).MustNotNullOrEmptyOrWhiteSpace().MustLengthInRange(1, 4);
        //年龄要么为null，要么>=0
        ctx.RuleFor(i => i.Age).When(i => i != null, ctx => ctx.MustGreaterThanOrEuqalTo(0));
        //出生日期要么为null,要么>=1800-01-01
        ctx.RuleFor(i => i.Birth).When(i => i != null, ctx => ctx.MustGreaterThanOrEuqalTo(DateTime.Parse("1800-01-01")));
        //校验身份证号
        ctx.RuleFor(i => i.IdCard).MustIdCard();
        //如果手机号码不为null就校验格式
        ctx.RuleFor(i => i.Phone).When(i => i != null, ctx => ctx.MustCellPhone());
        //如果邮箱不为null就校验格式
        ctx.RuleFor(i => i.Email).When(i => i != null, ctx => ctx.MustEmailAddress());
    }
}
```

#### 3.8 注册表

```csharp
public void Test2()
{
    var path = @"HKEY_CURRENT_USER\TestApplication\Res";
    //判断是否存在
    RegistryHelper.Exists(path);
    //删除项
    RegistryHelper.DeletePath(path);
    //设置值
    RegistryHelper.SetString(path, "name", "小明");
    //读取值
    var name = RegistryHelper.GetString(path, "name");
}
```



更多介绍，参考：https://gitee.com/jackletter/DotNetCommon/tree/master/docs