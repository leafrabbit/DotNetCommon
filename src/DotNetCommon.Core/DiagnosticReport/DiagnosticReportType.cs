namespace DotNetCommon.DiagnosticReport
{
    using System;

    /// <summary>
    /// 诊断报告类型
    /// </summary>
    [Flags]
    public enum DiagnosticReportType
    {
        /// <summary>
        /// 包含操作系统信息
        /// </summary>
        System = 1,

        /// <summary>
        /// 包含当前进程
        /// </summary>
        Process = 2,

        /// <summary>
        /// 包含磁盘信息
        /// </summary>
        Drives = 4,

        /// <summary>
        /// 包含所有加载的程序集信息
        /// </summary>
        Assemblies = 8,

        /// <summary>
        /// 包含环境变量
        /// </summary>
        EnvironmentVariables = 16,

        /// <summary>
        /// 包含所有网卡信息
        /// </summary>
        Networks = 32,

        /// <summary>
        /// 包含操作系统、当前进程、磁盘、程序集、环境变量和网卡信息
        /// </summary>
        Full = System | Process | Drives | Assemblies | EnvironmentVariables | Networks
    }
}