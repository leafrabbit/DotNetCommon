﻿namespace DotNetCommon.DiagnosticReport
{
    using System;

    /// <summary>
    /// 表示程序集信息
    /// </summary>
    public sealed class AssemblyDetails
    {
        /// <summary>
        /// 程序集全名
        /// </summary>
        public string Name { get; internal set; }

        /// <summary>
        /// 是否从GAC中加载
        /// </summary>
        public bool IsGAC { get; internal set; }

        /// <summary>
        /// 是否是64位
        /// </summary>
        public bool Is64Bit { get; internal set; }

        /// <summary>
        /// 是否是Release模式下生成的
        /// </summary>
        public bool IsOptimized { get; internal set; }

        /// <summary>
        /// 程序集对应的FrameWork版本
        /// </summary>
        public string Framework { get; internal set; }

        /// <summary>
        /// 程序集对应的文件路径,如: "C:\\windows\\Microsoft.Net\\assembly\\GAC_MSIL\\System\\v4.0_4.0.0.0__b77a5c561934e089\\System.dll"
        /// </summary>
        public string Location { get; internal set; }

        /// <summary>
        /// 程序集对应的文件地址,如: "file:///C:/windows/Microsoft.Net/assembly/GAC_MSIL/System/v4.0_4.0.0.0__b77a5c561934e089/System.dll"
        /// </summary>
        public Uri CodeBase { get; internal set; }
    }
}