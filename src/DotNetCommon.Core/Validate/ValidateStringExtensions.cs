﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using DotNetCommon.Extensions;

namespace DotNetCommon.Validate
{

    /// <summary>
    /// 字符串校验扩展
    /// </summary>
    public static class ValidateStringExtensions
    {
        /// <summary>
        /// 校验字符串必须为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustBeNull(this ValidateContext<string> ctx, string errorMessage = null)
        {
            if (ctx.Model != null)
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 必须为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 校验字符串必须为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustBeNullOrEmptyOrWhiteSpace(this ValidateContext<string> ctx, string errorMessage = null)
        {
            if (ctx.Model != null)
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 必须为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 校验字符串不能为空
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustNotNullOrEmptyOrWhiteSpace(this ValidateContext<string> ctx, string errorMessage = null)
        {
            if (ctx.Model.IsNullOrEmptyOrWhiteSpace())
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 不能为空!"));
            }
            return ctx;
        }

        /// <summary>
        /// 字符串长度范围
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="minLength">最小长度</param>
        /// <param name="maxLength">最大长度</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustLengthInRange(this ValidateContext<string> ctx, int? minLength = null, int? maxLength = null, string errorMessage = null)
        {
            if (ctx.Model == null)
            {
                if (minLength > 0)
                {
                    ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 的长度不能小于 {minLength}!"));
                }
            }
            else
            {
                if (minLength > 0 && ctx.Model.Length < minLength)
                {
                    ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 的长度不能小于 {minLength}!"));
                }
                if (maxLength > 0 && ctx.Model.Length > maxLength)
                {
                    ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 的长度不能大于 {maxLength}!"));
                }
            }
            return ctx;
        }

        /// <summary>
        /// 字符串编码后的字节数组长度范围
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="encoding">编码方式，如: Encoding.UTF8</param>
        /// <param name="minLength">最小长度</param>
        /// <param name="maxLength">最大长度</param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustByteLengthInRange(this ValidateContext<string> ctx, Encoding encoding, int? minLength = null, int? maxLength = null, string errorMessage = null)
        {
            if (ctx.Model == null)
            {
                if (minLength > 0)
                {
                    ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 的长度不能小于 {minLength}!"));
                }
            }
            else
            {
                var byteLen = encoding.GetBytes(ctx.Model).Length;
                if (minLength > 0 && byteLen < minLength)
                {
                    ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 编码后的长度不能小于 {minLength}!"));
                }
                if (maxLength > 0 && byteLen > maxLength)
                {
                    ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 编码后的长度不能大于 {maxLength}!"));
                }
            }
            return ctx;
        }

        /// <summary>
        /// 字符串中必须包含value
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="value"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustContais(this ValidateContext<string> ctx, string value, string errorMessage = null)
        {
            if (!(ctx.Model ?? "").Contains(value))
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 中必须包含 '{value}'!"));
            }
            return ctx;
        }

        /// <summary>
        /// 字符串不能包含value
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="value"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustNotContais(this ValidateContext<string> ctx, string value, string errorMessage = null)
        {
            if ((ctx.Model ?? "").Contains(value))
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 中不能包含 '{value}'!"));
            }
            return ctx;
        }

        /// <summary>
        /// 字符串必须匹配正则表达式
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="pattern"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustMatchRegex(this ValidateContext<string> ctx, string pattern, string errorMessage = null)
        {
            var reg = new Regex(pattern);
            if (!reg.IsMatch(ctx.Model))
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 格式错误!"));
            }
            return ctx;
        }

        /// <summary>
        /// 字符串必须是邮箱格式
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustEmailAddress(this ValidateContext<string> ctx, string errorMessage = null)
        {
            if (!ValidateHelper.IsValidEmail(ctx.Model))
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 不是正确的邮箱!"));
            }
            return ctx;
        }

        /// <summary>
        /// 字符串必须是身份证格式
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustIdCard(this ValidateContext<string> ctx, string errorMessage = null)
        {
            if (!ValidateHelper.IsChinaIdCard18(ctx.Model))
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 不是正确的身份证号!"));
            }
            return ctx;
        }

        /// <summary>
        /// 字符串必须是手机号码(兼容"+8615012341234"、"+86 15012341234"格式)
        /// </summary>
        /// <param name="ctx"></param>
        /// <param name="errorMessage"></param>
        /// <returns></returns>
        public static ValidateContext<string> MustCellPhone(this ValidateContext<string> ctx, string errorMessage = null)
        {
            if (!ValidateHelper.IsCellPhoneNumber(ctx.Model))
            {
                ctx.ErrorMessage.Add((ctx.ModelName, errorMessage ?? $"'{ctx.ModelPath}' 不是正确的手机号码!"));
            }
            return ctx;
        }
    }
}
