﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 分页查询请求
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    [DataContract]
    public class PageQuery
    {
        /// <summary>
        /// 请求的页码(约定: 从1开始),默认为1
        /// </summary>
        [DataMember(Order = 1)]
        public int PageIndex { get; set; } = 1;

        /// <summary>
        /// 请求的分页大小,默认为10
        /// </summary>
        [DataMember(Order = 2)]
        public int PageSize { get; set; } = 0;
    }
}
