﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 返回分页结果数据
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    [DataContract]
    public class ResultPage<T> : Result<Page<T>>
    {
        /// <summary>
        /// 隐示转换
        /// </summary>
        /// <param name="res"></param>
        public static implicit operator ResultPage<T>(Result res)
        {
            return new ResultPage<T>()
            {
                Code = res.Code,
                Message = res.Message,
                Data = new Page<T>()
                {
                    TotalCount = 0,
                    List = new List<T>()
                }
            };
        }
    }
}
