﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace DotNetCommon.Data
{
    /// <summary>
    /// 分页数据
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    [DataContract]
    public class Page<T>
    {
        /// <summary>
        /// 数据总量
        /// </summary>
        [DataMember(Order = 1)]
        public long TotalCount { get; set; }

        /// <summary>
        /// 当前页码内的数据
        /// </summary>
        [DataMember(Order = 2)]
        public IEnumerable<T> List { get; set; } = new List<T>();
    }
}
