﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetCommon
{
    /// <summary>
    /// 随机数帮助类
    /// </summary>
    public static class RandomHelper
    {
        static Random random = new Random(Guid.NewGuid().GetHashCode());

        /// <summary>
        /// 根据指定范围,生成一个随机整数 [min,max)
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns></returns>
        public static int Next(int min, int max)
        {
            Ensure.That(max >= min, "随机数的范围不合法!");
            return random.Next(min, max);
        }

        /// <summary>
        /// 生成一个 [0,max) 的随机整数
        /// </summary>
        /// <param name="max">最大整数(不包含)</param>
        /// <returns></returns>
        public static int Next(int max)
        {
            Ensure.That(max >= 0, "随机数的范围不合法!");
            return random.Next(0, max);
        }

        /// <summary>
        /// 根据指定的范围生成一个随机的小数 [min,max)
        /// </summary>
        /// <param name="min">最小值</param>
        /// <param name="max">最大值</param>
        /// <returns></returns>
        public static double Next(double min, double max)
        {
            Ensure.That(max >= min, "随机数的范围不合法!");
            return random.NextDouble() * (max - min) + min;
        }

        /// <summary>
        /// 生成一个小于max(不包含)的随机小数 [0,max)
        /// </summary>
        /// <param name="max">最大值(不包含)</param>
        /// <returns></returns>
        public static double Next(double max)
        {
            Ensure.That(max >= 0, "随机数的范围不合法!");
            return random.NextDouble() * max;
        }

    }
}
