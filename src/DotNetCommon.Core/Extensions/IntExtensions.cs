﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetCommon.Extensions
{
    using System.Collections.Generic;
    using System.Diagnostics;

    /// <summary>
    /// <see cref="long"/> 扩展类
    /// </summary>
    public static class Int64Extensions
    {
        /// <summary>
        /// 将纪元以来的毫秒转换为 <see cref="DateTime"/>
        /// </summary>
        /// <param name="epochMilliseconds">纪元以来的毫秒为 <see cref="long"/></param>
        /// <returns><see cref="DateTime"/></returns>
        public static DateTime FromEpochMilliseconds(this long epochMilliseconds) => DateTimeExtensions.Epoch.AddMilliseconds(epochMilliseconds);

        /// <summary>
        /// 返回一个 <see cref="IEnumerable{T}"/> ，其中包含 <paramref name="times"/> 项。
        /// </summary>
        /// <param name="times">结果中要包含的项目数</param>
        public static IEnumerable<long> Times(this long times)
        {
            for (long i = 1; i <= times; ++i) { yield return i; }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static void Times(this long times, Action<long> actionFn)
        {
            for (long index = 1; index <= times; ++index) { actionFn(index); }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static void Times(this long times, Action actionFn)
        {
            for (long index = 1; index <= times; ++index) { actionFn(); }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次，并返回每次执行的结果。
        /// </summary>
        /// <param name="times">应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static IReadOnlyList<T> Times<T>(this long times, Func<T> actionFn)
        {
            var list = new List<T>();
            for (long index = 1; index <= times; ++index) { list.Add(actionFn()); }
            return list;
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次，并返回每次执行的结果。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static IReadOnlyList<T> Times<T>(this long times, Func<long, T> actionFn)
        {
            var list = new List<T>();
            for (long index = 1; index <= times; ++index) { list.Add(actionFn(index)); }
            return list;
        }

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Ticks</c>。
        /// </summary>
        public static TimeSpan Ticks(this long number) => TimeSpan.FromTicks(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Milliseconds</c>。
        /// </summary>
        public static TimeSpan Milliseconds(this long number) => TimeSpan.FromMilliseconds(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Seconds</c>。
        /// </summary>
        public static TimeSpan Seconds(this long number) => TimeSpan.FromSeconds(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Minutes</c>。
        /// </summary>
        public static TimeSpan Minutes(this long number) => TimeSpan.FromMinutes(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Hours</c>。
        /// </summary>
        public static TimeSpan Hours(this long number) => TimeSpan.FromHours(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Days</c>。
        /// </summary>
        public static TimeSpan Days(this long number) => TimeSpan.FromDays(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Weeks</c>。
        /// </summary>
        public static TimeSpan Weeks(this long number) => TimeSpan.FromDays(number * 7);
    }

    /// <summary>
    /// <see cref="int"/>的扩展方法
    /// </summary>
    public static class Int32Extensions
    {
        /// <summary>
        /// 返回一个 <see cref="IEnumerable{T}"/> ，其中包含 <paramref name="times"/> 项。
        /// </summary>
        /// <param name="times">结果中要包含的项目数</param>
        public static IEnumerable<int> Times(this int times)
        {
            for (var i = 1; i <= times; ++i) { yield return i; }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static void Times(this int times, Action<int> actionFn)
        {
            for (var index = 1; index <= times; ++index) { actionFn(index); }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static void Times(this int times, Action actionFn)
        {
            for (var index = 1; index <= times; ++index) { actionFn(); }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次，并返回每次执行的结果。
        /// </summary>
        /// <param name="times">应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static IReadOnlyList<T> Times<T>(this int times, Func<T> actionFn)
        {
            var list = new List<T>();
            for (var index = 1; index <= times; ++index) { list.Add(actionFn()); }
            return list;
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次，并返回每次执行的结果。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static IReadOnlyList<T> Times<T>(this int times, Func<int, T> actionFn)
        {
            var list = new List<T>();
            for (var index = 1; index <= times; ++index) { list.Add(actionFn(index)); }
            return list;
        }

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Ticks</c>。
        /// </summary>
        public static TimeSpan Ticks(this int number) => TimeSpan.FromTicks(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Milliseconds</c>。
        /// </summary>
        public static TimeSpan Milliseconds(this int number) => TimeSpan.FromMilliseconds(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Seconds</c>。
        /// </summary>
        public static TimeSpan Seconds(this int number) => TimeSpan.FromSeconds(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Minutes</c>。
        /// </summary>
        public static TimeSpan Minutes(this int number) => TimeSpan.FromMinutes(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Hours</c>。
        /// </summary>
        public static TimeSpan Hours(this int number) => TimeSpan.FromHours(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Days</c>。
        /// </summary>
        public static TimeSpan Days(this int number) => TimeSpan.FromDays(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Weeks</c>。
        /// </summary>
        public static TimeSpan Weeks(this int number) => TimeSpan.FromDays(number * 7);
    }

    /// <summary>
    /// Extension methods for <see cref="short"/>
    /// </summary>
    public static class Int16Extensions
    {
        /// <summary>
        /// Returns an <see cref="IEnumerable{T}"/> containing <paramref name="times"/> item.
        /// </summary>
        /// <param name="times">The number of items to include in the result</param>
        public static IEnumerable<short> Times(this short times)
        {
            for (short i = 1; i <= times; ++i) { yield return i; }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static void Times(this short times, Action<short> actionFn)
        {
            for (short index = 1; index <= times; ++index) { actionFn(index); }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static void Times(this short times, Action actionFn)
        {
            for (short index = 1; index <= times; ++index) { actionFn(); }
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次，并返回每次执行的结果。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static IReadOnlyList<T> Times<T>(this short times, Func<T> actionFn)
        {
            var list = new List<T>();
            for (short index = 1; index <= times; ++index) { list.Add(actionFn()); }
            return list;
        }

        /// <summary>
        /// 执行给定的 <paramref name="actionFn"/> <paramref name="times"/> 次，并返回每次执行的结果。
        /// </summary>
        /// <param name="times"><paramref name="actionFn"/> 应该执行的次数</param>
        /// <param name="actionFn">要执行的动作</param>
        public static IReadOnlyList<T> Times<T>(this short times, Func<short, T> actionFn)
        {
            var list = new List<T>();
            for (short index = 1; index <= times; ++index) { list.Add(actionFn(index)); }
            return list;
        }

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Ticks</c>。
        /// </summary>
        public static TimeSpan Ticks(this short number) => TimeSpan.FromTicks(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Milliseconds</c>。
        /// </summary>
        public static TimeSpan Milliseconds(this short number) => TimeSpan.FromMilliseconds(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Seconds</c>。
        /// </summary>
        public static TimeSpan Seconds(this short number) => TimeSpan.FromSeconds(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Minutes</c>。
        /// </summary>
        public static TimeSpan Minutes(this short number) => TimeSpan.FromMinutes(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Hours</c>。
        /// </summary>
        public static TimeSpan Hours(this short number) => TimeSpan.FromHours(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Days</c>。
        /// </summary>
        public static TimeSpan Days(this short number) => TimeSpan.FromDays(number);

        /// <summary>
        /// 返回一个 <see cref="TimeSpan"/> 由 <paramref name="number"/> 表示为 <c>Weeks</c>。
        /// </summary>
        public static TimeSpan Weeks(this short number) => TimeSpan.FromDays(number * 7);
    }
}
