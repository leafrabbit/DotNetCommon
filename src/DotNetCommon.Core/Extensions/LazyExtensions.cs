﻿namespace DotNetCommon.Extensions
{
    using System;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;

    /// <summary>
    /// <see cref="Lazy{T}"/>扩展类
    /// </summary>
    public static class LazyExtensions
    {
        /// <summary>
        /// 允许 <c>Lazy&lt;Task&lt;T>></c> 前用 <c>await</c> 关键字
        /// </summary>
        public static TaskAwaiter<T> GetAwaiter<T>(this Lazy<Task<T>> lazy) => lazy.Value.GetAwaiter();
    }
}