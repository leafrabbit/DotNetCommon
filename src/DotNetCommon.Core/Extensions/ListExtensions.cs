﻿namespace DotNetCommon.Extensions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    /// <summary>
    /// <see cref="IList{T}"/> 的扩展方法
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// 主要用来做List的初始化: <code>new List&lt;int>{1,new int[]{2,3},4}</code>
        /// </summary>
        /// <returns>返回自身</returns>
        public static IList<T> Add<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (list == null) return list;
            foreach (var item in items)
            {
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// 将给定的<paramref name="items"/>添加到给定的<paramref name="list"/>。
        /// </summary>
        /// <returns>返回自身</returns>
        public static IList<T> AddRange<T>(this IList<T> list, IEnumerable<T> items)
        {
            if (list == null) return list;
            foreach (var item in items)
            {
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// 从指定的集合中移除符合条件的元素
        /// </summary>
        /// <returns>返回自身</returns>
        public static IList<T> Remove<T>(this IList<T> list, Func<T, bool> predicate)
        {
            if (list == null) return list;
            list.Where(predicate).ToList().ForEach(t =>
            {
                if (list.Contains(t)) list.Remove(t);
            });
            return list;
        }
    }
}