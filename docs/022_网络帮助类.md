# 网络转换器

> 所属包: DotNetCommon.Core



这里封装了类```NetworkHelper```，可以方便的获取网络信息。

使用示例：



```csharp
 //输出: DESKTOP-JACKLETTER.
var name = NetworkHelper.GetFQDN();

//输出: 192.168.0.6
var ip = NetworkHelper.GetLocalIPAddress();

//输出:
/*
{[{192.168.0.6}, 以太网]}
{[{192.168.222.1}, VMware Network Adapter VMnet1]}
{[{192.168.186.1}, VMware Network Adapter VMnet8]}
*/
var ips = NetworkHelper.GetLocalIPAddresses();
```


