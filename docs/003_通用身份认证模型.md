# 通用身份认证模型

> 所属包: DotNetCommon.Core


## 1. 问题来源
在```asp.net core```开发中，我们经常需要获取当前登录者的身份，一般我们是通过```IHttpContextAccessor```，去获取当前请求的```HttpContext```，进而获取当前登录者Id的，但很多时候我们的业务代码中不能持有web库的引用，造成无法直接访问```HttpContext```。另外，我们经常需要在一个请求逻辑中共享数据， ```asp.net core```的```HttpContex.Items```提供了这样的一个容器，但很多时候我们的业务代码中不能持有web库的引用，造成无法直接访问。

为了解决上面的问题，在```DotNetCommon.Core```包中定义了```DotNetCommon.User```对象，这个对象本身不存储信息，而是提供一个注入的逻辑，这样，我们在其他非Web依赖的项目中就可以使用了。


## 2. User对象的定义

简化定义如下：

```csharp
namespace DotNetCommon
{
    public class User
    {
	     //用户Id，定义为string是为了兼容
		 public string IdString { get; }
         //是否经过认证，注意：只是认证用户身份，和授权没关系
         public bool IsAuthenticated { get; }     
         //代码上下文字典（可配置为一个请求一个）
         public IDictionary<object,object> Properties { get; }     
        
         //注入用户Id获取逻辑
         public static void RegisterIdString(Func<string> getIdString, bool isCache = true);         
         //注入用户是否认证的判别逻辑
         public static void RegisterIsAuthenticated(Func<bool> isAuthenticated, bool isCache = true);
         //注入代码上下文容器
         public static void RegisterPropertiesDictionary(Func<IDictionary<object, object>> getProperties, bool isCache = true);
    }
}
```



## 3. User对象使用示例

当我们准备使用它的时候，可以参考如下代码:

```csharp
//startup.cs
public void ConfigureServices(IServiceCollection services)
{
    //首先将IHttpContextAccessor加入容器
    services.AddHttpContextAccessor();
}
public void Configure(IApplicationBuilder app)
{
     //依次注入数据获取逻辑
     IHttpContextAccessor httpContextAccessor = app.ApplicationServices.GetRequiredService<IHttpContextAccessor>(); ;
     DotNetCommon.User.RegisterIdString(() => httpContextAccessor.HttpContext.User.Identity.Name);
     DotNetCommon.User.RegisterIsAuthenticated(() => httpContextAccessor.HttpContext.User.Identity.IsAuthenticated);
     // 考虑非web环境使用，如: 开启一个消息后台主机服务(IHostedService)去订阅消息队列
     DotNetCommon.User.RegisterPropertiesDictionary(() => (httpContextAccessor.HttpContext?.Items) ?? new Dictionary<object, object>());
}

//业务代码
public class PersonService:IPersonService
{
    public string GetCurrentUser()
    {
        if(DotNetCommon.User.Current.IsAuthenticated)
        {
            return DotNetCommon.User.Current.IdString;
        }
        else
        {
         	throw new Exception("尚未登录");
        }
    }
}
```



## 4. 关于 User.Properties

可以将```HttpContext.Items```注入到```User.Properties```中，这样就可以很方便的在请求上下文中共享数据了。

除了可以用 ```User.Properties``` 拿到注入的字典外，我们还可以直接调用 ```User.GetProperty()```、```User.SetProperty```等读写共享的数据。



上面示例代码中用户字典的注入做个非Web环境的兼容，兼容```User.SetProperty```等方法也能很好的应用到各个异步代码块中而不会相互干扰。

其实，```User.Current```本身是基于 ```AsyncLocal<User>``` 实现的，关于```AsyncLocal```的使用可参考：[《c#: AsyncLocal的使用》](https://blog.csdn.net/u010476739/article/details/121395350)

