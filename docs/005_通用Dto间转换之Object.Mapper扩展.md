# 通用Dto间转换之Object.Mapper扩展

> 所属包: DotNetCommon.Core



## 1. 简介

该功能是类比AutoMapper实现的不用类转换的功能。

因为在使用AutoMapper中发现它的转换性能慢，后来一直没找到解决办法，所以就自己写了一个Mapper扩张方法实现不同类之间转换。

它的特点如下：

- 基于反射实现；
- 能够转换没有任何关系的两个类，只要它们的属性名和属性类型相同即可（集合、数组可以）；
- 转换中能够处理循环引用的情况；
- 整个实现代码轻量，共200行代码；



## 2. 不同类之间的简单转换

> 注意： Mapper方法是在Object上做的扩展，所以使用前先引入命名空间： DotNetCommon.Extensions



类模型：

```csharp
public class Cat
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age { get; set; }
    public DateTime Birth { get; set; }
}

public class CatDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Age
    {
        get
        {
            return DateTime.Now.Year - Birth.Year;
        }
    }
    public DateTime Birth { get; set; }
}
```



转换示例：

```csharp
var cat = new Cat()
{
    Id = 1,
    Name = "小明",
    Birth = DateTime.Parse("1989-01-02"),
    Age = 20
};
var dto = cat.Mapper<CatDto>();
dto.ShouldNotBeNull();
dto.Id.ShouldBe(1);
dto.Name.ShouldBe("小明");
dto.Age.ShouldNotBe(20);
```



## 3. 循环引用

类模型：

```csharp
 public class PersonCircle
 {
     public int Id { get; set; }
     public string Name { get; set; }
     public string Pwd { get; set; }
     public BookCircle Book { set; get; }
 }

public class BookCircle
{
    public int Id { get; set; }
    public string Name { get; set; }
    public PersonCircle Person { get; set; }
}

public class PersonCircleDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public BookCircleDto Book { set; get; }
}

public class BookCircleDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public PersonCircleDto Person { get; set; }
}
```



转换示例：

```csharp
List<PersonCircle> list = new List<PersonCircle>()
{
    new PersonCircle()
    {
        Id=1,
        Name="小明",
        Pwd="xiaopming",
        Book=new BookCircle()
        {
            Id=1,
            Name="语文"
        }
    },
    new PersonCircle()
    {
        Id=1,
        Name="小王",
        Pwd="wang",
        Book=new BookCircle()
        {
            Id=2,
            Name="数学"
        }
    }
};
list[0].Book.Person = list[0];
list[1].Book.Person = list[1];
var dtos = list.Mapper<List<PersonCircleDto>>();
dtos.ShouldNotBeNull();
dtos.Count.ShouldBe(2);
dtos[0].Name.ShouldBe("小明");
dtos[0].Book.Person.ShouldBe(dtos[0]);
dtos[1].Name.ShouldBe("小王");
dtos[1].Book.Person.ShouldBe(dtos[1]);
```



## 4. 树状结构

类模型：

```csharp
public class Org
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Pwd { get; set; }
    public DateTime CreatetTime { get; set; }
    public bool? Active { get; set; }
    public int? ParentId { get; set; }

    public List<Org> Children { get; set; }
}

public class OrgDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public bool? Active { get; set; }

    public List<OrgDto> Children { get; set; }
}
```



转换示例：

```csharp
List<Org> list = null;
#region 构造平铺的数据
list = new List<Org>()
{
    new Org()
    {
        Id=1,
        Name="河南",
        Pwd="henan",
        CreatetTime=DateTime.Now,
        Active=null
    },
    new Org()
    {
        Id=2,
        Name="河北",
        Pwd="hebei",
        CreatetTime=DateTime.Now,
        Active=null
    },
    new Org()
    {
        Id=3,
        Name="郑州",
        ParentId=1,
        Active=true,
        CreatetTime=DateTime.Now,
        Pwd="zhengzhou"
    },
    new Org()
    {
        Id=4,
        Name="开封",
        ParentId=1,
        Active=true,
        CreatetTime=DateTime.Now,
        Pwd="kaifeng"
    },
    new Org()
    {
        Id=5,
        Name="中原区",
        ParentId=3,
        Active=true,
        CreatetTime=DateTime.Now,
        Pwd="zhongyuanqu"
    },
    new Org()
    {
        Id=6,
        Name="金水区",
        ParentId=3,
        Active=true,
        CreatetTime=DateTime.Now,
        Pwd="jinshuiqu"
    },
    new Org()
    {
        Id=7,
        Name="文化路",
        ParentId=6,
        Active=true,
        CreatetTime=DateTime.Now,
        Pwd="wenhualu"
    }
};
#endregion
var orgs = list.FetchToTree(o => o.Id, o => o.ParentId);
var dtos = orgs.Mapper<List<OrgDto>>();

dtos.ShouldNotBeNull();
dtos.Count.ShouldBe(2);
dtos[0].Name.ShouldBe("河南");
dtos[1].Name.ShouldBe("河北");
dtos[0].Children.ShouldNotBeNull();
dtos[0].Children.Count.ShouldBe(2);
dtos[0].Children[0].Children[1].Name.ShouldBe("金水区");
dtos[0].Children[0].Children[1].Children[0].Name.ShouldBe("文化路");
```



## 5. 含集合属性的转换

类模型：

```csharp
public class Collect2
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<Collect2Sub> Subs { get; set; }
    public IEnumerable<Collect2Sub> SubsEnumerable { get; set; }
    public IList<Collect2Sub> SubsIList { get; set; }
    public Collect2Sub[] CollectSubArr { get; set; }
}
public class Collect2Sub
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Score { get; set; }
}

public class Collect2Dto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public List<Collect2SubDto> Subs { get; set; }
    public IEnumerable<Collect2SubDto> SubsEnumerable { get; set; }
    public IList<Collect2SubDto> SubsIList { get; set; }
    public Collect2SubDto[] CollectSubArr { get; set; }
}
public class Collect2SubDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Score { get; set; }
}
```



转换示例：

```csharp
var col = new Collect2()
{
    Id = 1,
    Name = "测试",
    Subs = new List<Collect2Sub>()
    {
        new Collect2Sub()
        {
            Id=1,
            Name="sub1",
            Score=12
        },
        new Collect2Sub()
        {
            Id=2,
            Name="sub2",
            Score=88
        }
    },
    SubsEnumerable = new List<Collect2Sub>()
    {
        new Collect2Sub()
        {
            Id=1,
            Name="sub3",
            Score=12
        },
        new Collect2Sub()
        {
            Id=2,
            Name="sub4",
            Score=88
        }
    },
    SubsIList = new List<Collect2Sub>()
    {
        new Collect2Sub()
        {
            Id=1,
            Name="sub5",
            Score=12
        },
        new Collect2Sub()
        {
            Id=2,
            Name="sub6",
            Score=88
        }
    },
    CollectSubArr = new List<Collect2Sub>()
    {
        new Collect2Sub()
        {
            Id=1,
            Name="sub5",
            Score=12
        },
        new Collect2Sub()
        {
            Id=2,
            Name="sub6",
            Score=88
        }
    }.ToArray()
};
var dto = col.Mapper<Collect2Dto>();
dto.ShouldNotBeNull();
dto.Name.ShouldBe("测试");
dto.Subs.ShouldNotBeNull();
dto.Subs.Count.ShouldBe(2);

dto.SubsEnumerable.ShouldNotBeNull();
dto.SubsEnumerable.ToList().Count.ShouldBe(2);
dto.SubsEnumerable.ToList()[0].Name.ShouldBe("sub3");

dto.SubsIList.ShouldNotBeNull();
dto.SubsIList.Count.ShouldBe(2);
dto.SubsIList[0].Name.ShouldBe("sub5");

dto.CollectSubArr.ShouldNotBeNull();
dto.CollectSubArr.Length.ShouldBe(2);
dto.CollectSubArr[1].Name.ShouldBe("sub6");
```

